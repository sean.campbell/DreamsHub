#include "driverlib.h"
#include "LOG_DREAMS.h"
#include "DS1307.h"
#include "clocking_dreams.h"

#define LOG_1_NAME "data"
#define LOG_1_PATH "/logs/data"


int main(void) 
{
	int res;

    WDT_A_holdTimer();
    GPIO_setAsOutputPin(GPIO_PORT_P9,GPIO_PIN3);
    GPIO_setOutputLowOnPin(GPIO_PORT_P9,GPIO_PIN3);

    res = log_init();

    setupClocks();
    res = log_createNew(LOG_1_PATH, LOG_1_NAME);


    while(true){
        res =  log_writeLine(LOG_1_NAME, getStringTime());

    	// IF all data is written, light LED.
    	if (res > 0) {
    		MAP_GPIO_toggleOutputOnPin(GPIO_PORT_P9,GPIO_PIN3);
    	}

    	timer32_delay(2000);
    }
}
