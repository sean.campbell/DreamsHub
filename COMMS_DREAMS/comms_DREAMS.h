/*
 * comms_DREAMS.h
 *
 *  Created on: 21 Mar 2016
 *      Author: Sean
 */

#ifndef COMMS_DREAMS_H_
#define COMMS_DREAMS_H_

#include <stdbool.h>
#include <stdint.h>

// Avoid passing an additional variable around on the stack
extern uint_fast32_t HUB_ID;

void comms_setupProtocol(char *sendBuffer, uint_fast32_t maxSendLen);

bool comms_sendReceiveMessage(char *sendBuffer, uint_fast32_t maxSendLen);

void comms_sendEmergencySMS(uint_fast32_t sensorId, float threshold, float value);

#endif /* COMMS_DREAMS_H_ */
