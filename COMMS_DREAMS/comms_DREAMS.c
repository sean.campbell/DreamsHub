/*
 * comms_DREAMS.c
 *
 *  Created on: 21 Mar 2016
 *      Author: Sean
 */

#include "comms_DREAMS.h"
#include "modem_DREAMS.h"

#include "adc_DREAMS.h"	// For sensor config editing
#include "hub_DREAMS.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <float.h>
#include <string.h>

#include "frozen.h"	// 3rd Party

#ifndef NULL
#define NULL ((void*)0)
#endif

// Buffer size for receiving messages
#define TCP_RECEIVE_BUFFER_SIZE		256

// Define the JSON keys that will be searched for
#define JSON_KEY_SENSOR_CONFIG 				"SensorConfig"
#define JSON_KEY_SENSOR_CONFIG_DESC 		"SensorConfig[%d]"
#define JSON_KEY_SENSOR_CONFIG_COMPLETE 	"ConfigComplete"
#define JSON_KEY_SENSOR_CONFIG_COMPLETE_TS	"TimeStamp"

#define JSON_KEY_CONTACTS_CONFIG_ADD		"addContact"
#define JSON_KEY_CONTACTS_CONFIG_NAME		"name"
#define JSON_KEY_CONTACTS_CONFIG_NUMBER		"phone"
#define JSON_KEY_CONTACTS_CONFIG_COMPLETE 	"ContactsComplete"


#define JSON_KEY_TOTAL	4

typedef enum {
	SENSOR_CONFIG_SUCCESS = 0,
	SENSOR_CONFIG_FAILURE,
	CONTACT_CONFIG_SUCCESS,
	CONTACT_CONFIG_FAILURE,
	NONE
} JSON_Parse_Response;

static bool parseBoolToken(struct json_token *arr, char *desc, int_fast32_t param){
	bool ret = false;
	struct json_token *temp;
	if(param != -1){
		char buffer[32];
		snprintf(buffer, sizeof(buffer), desc, param);
		desc = buffer;
	}
	temp = find_json_token(arr, desc);

	if(temp != NULL && temp->type == JSON_TYPE_TRUE){
		ret = true;
	}
	return ret;
}

static float parseFloatToken(struct json_token *arr, char *desc, int_fast32_t param, float defVal){
	float ret = defVal;
	struct json_token *temp;
	char buffer[32];
	if(param != -1){
		snprintf(buffer, sizeof(buffer), desc, param);
		desc = buffer;
	}
	temp = find_json_token(arr, desc);

	if(temp != NULL && temp->type == JSON_TYPE_NUMBER){
		strncpy(buffer, temp->ptr, temp->len);
		buffer[temp->len] = '\0';
		ret = atof(buffer);
	}
	return ret;
}

// Remember to release
static char *parseStringToken(struct json_token *arr, char *desc, int_fast32_t param){
	char *ret = NULL;
	struct json_token *temp;
	if(param != -1){
		char buffer[32];
		snprintf(buffer, sizeof(buffer), desc, param);
		desc = buffer;
	}
	temp = find_json_token(arr, desc);

	if(temp != NULL && temp->type == JSON_TYPE_STRING){
		ret = malloc((temp->len + 1)*sizeof(char));
		if(ret != NULL){
			strncpy(ret, temp->ptr, temp->len);
			ret[temp->len] = '\0';
		}
	}
	return ret;
}

static JSON_Parse_Response processSensorConfig(struct json_token *arr){
	JSON_Parse_Response ret = SENSOR_CONFIG_FAILURE;
	char buffer[32];
	struct json_token *temp;

	// Order "id", "enabled", "lowerThreshold", "multiplier", "offset", "sampleRate", "upperThreshold"
	// Get the first descendent and assume it's the id
	snprintf(buffer, sizeof(buffer), JSON_KEY_SENSOR_CONFIG_DESC, 0);
	temp = find_json_token(arr, buffer); // Find the token
	strncpy(buffer, temp->ptr, temp->len);	// Copy only said token across
	buffer[temp->len] = '\0';	// Null terminate

	adcSensorSettings *settings = adc_getSensorSettings(atoi(buffer));
	if(settings != NULL){
		// Get the next descendent and assume it's the enabled flag
		settings->sensorEn = parseBoolToken(arr, JSON_KEY_SENSOR_CONFIG_DESC, 1);

		// Get the next descendent and assume its the lower threshold
		settings->lowerThreshold = parseFloatToken(arr, JSON_KEY_SENSOR_CONFIG_DESC, 2, -FLT_MAX);

		// Get the next descendent and assume it's the multiplier
		settings->multiplier = parseFloatToken(arr, JSON_KEY_SENSOR_CONFIG_DESC, 3, 1.0);

		// Get the next descendent and assume it's the offset
		settings->offset = parseFloatToken(arr, JSON_KEY_SENSOR_CONFIG_DESC, 4, 0.0);

		// Get the next descendent and assume it's the sample rate
		settings->sampleRate = ((uint_fast32_t)parseFloatToken(arr, JSON_KEY_SENSOR_CONFIG_DESC, 5, 60.0));

		// Get the next descendent and assume it's the upper threshold
		settings->upperThreshold = parseFloatToken(arr, JSON_KEY_SENSOR_CONFIG_DESC, 6, FLT_MAX);

		// Reset the emergency information
		settings->belowLowerThreshold = false;
		settings->aboveUpperThreshold = false;

		// Inform the hub of the updated settings
		ret = (hub_processADCUpdate(settings)) ? SENSOR_CONFIG_SUCCESS : SENSOR_CONFIG_FAILURE;
	}
	return ret;
}

// On sever configuration complete:
//		- set the RTC
//		- write the ADC settings to flash		TODO
static JSON_Parse_Response processConfigComplete(struct json_token *arr){
	char *timestampISO = parseStringToken(arr, JSON_KEY_SENSOR_CONFIG_COMPLETE_TS, -1);
	if(timestampISO != NULL){
		hub_processTimeStampUpdate(timestampISO);
		free(timestampISO);
	}
	return NONE;
}

static JSON_Parse_Response processContactConfig(struct json_token *arr){
	JSON_Parse_Response ret = CONTACT_CONFIG_FAILURE;
	char *name = NULL, *number = NULL;

	// Order "addContact", "name", "phone"
	name = parseStringToken(arr, JSON_KEY_CONTACTS_CONFIG_NAME, -1);
	if(NULL != name){	// If name parsed successfully, continue
		number = parseStringToken(arr, JSON_KEY_CONTACTS_CONFIG_NUMBER, -1);

		if(NULL != number){ // If number parsed successfully, continue

			if(parseBoolToken(arr, JSON_KEY_CONTACTS_CONFIG_ADD, -1)){
				ret = (modem_addContact(number, name, false)) ? CONTACT_CONFIG_SUCCESS :
						CONTACT_CONFIG_FAILURE;
			}
			else{	// Assume removeContact
				ret = (modem_removeContact(name)) ? CONTACT_CONFIG_SUCCESS :
						CONTACT_CONFIG_FAILURE;
			}
			free(number);
		}
		free(name);
	}
	return ret;
}

static JSON_Parse_Response processContactComplete(struct json_token *arr){
	return NONE;
}

// Build compile time array of keys
static char *json_delims[JSON_KEY_TOTAL] =	{JSON_KEY_SENSOR_CONFIG,
												JSON_KEY_CONTACTS_CONFIG_ADD,
												JSON_KEY_SENSOR_CONFIG_COMPLETE,
												JSON_KEY_CONTACTS_CONFIG_COMPLETE};

// The key processing functions; use the bool flag to indicate if response necessary
#define JSON_SENSOR_CONFIG_SUCCESS 		"{\"SiteID\":%d, \"ConfigSuccess\":true}"
#define JSON_SENSOR_CONFIG_FAILURE 		"{\"SiteID\":%d, \"ConfigSuccess\":false}"
#define JSON_CONTACTS_CONFIG_SUCCESS 	"{\"SiteID\":%d, \"ContactSuccess\":true}"
#define JSON_CONTACTS_CONFIG_FAILURE 	"{\"SiteID\":%d, \"ContactSuccess\":false}"
// Build compile time array of responses
static char *json_responses[] = {JSON_SENSOR_CONFIG_SUCCESS,
									JSON_SENSOR_CONFIG_FAILURE,
									JSON_CONTACTS_CONFIG_SUCCESS,
									JSON_CONTACTS_CONFIG_FAILURE
								};

// Build compile time array of function pointers
static JSON_Parse_Response (*json_funcs[JSON_KEY_TOTAL])(struct json_token *arr)
										= 	{processSensorConfig,
											processContactConfig,
											processConfigComplete,
											processContactComplete};

static JSON_Parse_Response interpretJSONMessage(char *receiveBuffer){
	JSON_Parse_Response ret = NONE;
	uint_fast8_t idx;
	struct json_token *arr, *tok = NULL;

	// Interpret the message
	arr = parse_json2(receiveBuffer, strlen(receiveBuffer));

	// Start looking through the parsed message for expected keys
	for(idx = 0; idx != JSON_KEY_TOTAL; idx++){
		tok = find_json_token(arr, json_delims[idx]);
		if(tok != NULL){
			ret = (*json_funcs[idx])(arr);
			break;
		}
	}
	free(arr); // Remember to release resources
	return ret;
}

static bool sendReceiveSingleMessage(char *sendBuffer, char *receiveBuffer, uint_fast32_t maxReceiveLen){
	bool ret = false;
	uint_fast16_t retries = 0;

	modem_sendTCPMessage(sendBuffer);
	while(!ret && retries < 3){
		ret = modem_getTCPMessage(receiveBuffer, maxReceiveLen);
		retries++;
	}
	return ret;
}

// Public functions
static char tcpReceiveBuffer[TCP_RECEIVE_BUFFER_SIZE];

#define COMMS_SETUP_MESSAGE "{\"SiteID\":%d, \"ConfigRequest\":true}"
void comms_setupProtocol(char *sendBuffer, uint_fast32_t maxSendLen){
	// Use the opening message
	snprintf(sendBuffer, maxSendLen, COMMS_SETUP_MESSAGE, HUB_ID);
	comms_sendReceiveMessage(sendBuffer, maxSendLen);
}

bool comms_sendReceiveMessage(char *sendBuffer, uint_fast32_t maxSendLen){
	bool rec = sendReceiveSingleMessage(sendBuffer, tcpReceiveBuffer, TCP_RECEIVE_BUFFER_SIZE);
	JSON_Parse_Response resp = NONE;
	while(rec){
		resp = interpretJSONMessage(tcpReceiveBuffer);
		if(resp != NONE){
			snprintf(sendBuffer, maxSendLen, json_responses[resp], HUB_ID);
			rec = sendReceiveSingleMessage(sendBuffer, tcpReceiveBuffer, TCP_RECEIVE_BUFFER_SIZE);
		}
		else{
			rec = false;
		}
	}
	return rec;
}

#define EMERGENCY_SMS_ABOVE "Warning: Sensor%d at SiteID %d exceeded maximum threshold of %.5f with value %.5f"
#define EMERGENCY_SMS_BELOW "Warning: Sensor%d at SiteID %d below minimum threshold of %.5f with value %.5f"
void comms_sendEmergencySMS(uint_fast32_t sensorId, float threshold, float value){
	snprintf(tcpReceiveBuffer, TCP_RECEIVE_BUFFER_SIZE,
			(value < threshold) ? EMERGENCY_SMS_BELOW : EMERGENCY_SMS_ABOVE,
					sensorId, HUB_ID, threshold, value);
	modem_sendEmergencySMS(tcpReceiveBuffer);
}

