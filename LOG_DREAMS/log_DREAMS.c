/*
 * Logging.c
 *
 *  Created on: 30 Jan 2016
 *      Author: David
 */

#include "log_DREAMS.h"
#include "DS1307.h"

#include <stdlib.h>
#include <string.h>


// Fastfs working area
FATFS FatFs;

// Array of logging types.
struct logTypeS logTypeList[MAX_N_LOGS];
FIL files[MAX_N_LOGS];

// Number of logging types
int nTypes;

bool cardExist = false;

FRESULT buildPath(char* path) {
	FRESULT res;
	char* localCopy;

	// Find length of path
	int len = strlen(path);

	// Build a local non-constant array
	localCopy = malloc(len + 1);
	strcpy(localCopy, path);

	// Current position within path
	int pos = 1;

	// Iterate until end of path
	while (pos < len) {

		// Find next '/' char or the end of string
		while (localCopy[++pos] != '/' & localCopy[pos] != '\0');
		// Replace the next '/' with end of string char
		localCopy[pos] = '\0';

		// Attempt to make this path
		res = f_mkdir(localCopy);

		// Make sure there was no error
		if ((res != FR_EXIST) & (res != FR_OK))
			break;

		// hopefully replace '/' char
		localCopy[pos] = '/';
	}

	free(localCopy);
	return res;
}

bool initFs(){
	// Initialise working area, and check if successful
	if(f_mount(&FatFs, "", 1) == FR_OK){
		cardExist = true;
		return true;
	}
	else{
		cardExist = false;
		return false;
	}
}

int log_init() {
	nTypes = 0;

	initRTC();

	return initFs();
}


int log_createNew(char* path, char* logName) {

	if ((strlen(path) > MAX_PATH) || (strlen(logName) > MAX_NAME)) {
		return -1;
	}
	// Ensure that there is space left in the array
	if (nTypes < MAX_N_LOGS) {

		// Initialise log type variables
		strcpy(logTypeList[nTypes].path, path);
		logTypeList[nTypes].pathLen = strlen(path);
		strcpy(logTypeList[nTypes].logName, logName);
		logTypeList[nTypes].nameLen = strlen(logName);
		logTypeList[nTypes].open = false;
		// Increment number of logtypes
		nTypes++;
	} else {
		return -1;
	}

	return nTypes;
}

logTypeS* findLog(char* logName) {
	// Iterate through array of logs
	int i;
	for(i = 0; i < nTypes; i++) {
		// find log in question
		if (!strcmp(logTypeList[i].logName, logName)) {
			return &logTypeList[i];
		}
	}
	return NULL;
}


FRESULT safeOpenFile(logTypeS* log){

	FRESULT res = FR_INT_ERR;
	char *fullPath;

	// Check that the path exists & create
	res = buildPath(log->path);

	if (res == FR_OK | FR_EXIST) {
		// Try and open file

		int fullPathSize = log->pathLen + 10 + log->nameLen;
		fullPath = (char *) malloc(fullPathSize);

		if(fullPath != NULL){

			int index = 0;

			while(true){

				snprintf(fullPath, fullPathSize, "%s/%s%02d.log", log->path, log->logName, index);

				res = f_open(&log->file, fullPath, FA_WRITE | FA_OPEN_ALWAYS);

				if(res != FR_OK) return res;
				else if(f_size(&log->file) < MAX_FILE_SIZE) break;

				index++;
				if(index >= 99) return FR_INT_ERR;

			}
			// Reduce critical path for file creation
			f_sync(&log->file);

			res = f_lseek(&log->file, f_size(&log->file));

			if (res == FR_OK) {
				log->open = true;
			}
		}
		free(fullPath);
	}

	return res;
}

int log_writeLine(char* logName, char * message) {
	// bytes written
	UINT bw = 0;
	// bytes to write;
	UINT bTw = 0;
	// Result from file writing attempt
	FRESULT res = FR_OK;

	// Find the log in question
	logTypeS* log = findLog(logName);

	// Check to see if file is already open
	if(!cardExist){
		initFs();
	}

	if(cardExist){
		if(!log->open){
			res = safeOpenFile(log);
		}
		/// Check if day number has changed
		else if(f_size(&log->file) > MAX_FILE_SIZE){
			f_close(&log->file);
			log->open = false;
			res = safeOpenFile(log);
		}

		// Check file opened.
		else{
			// Find length of log message
			bTw = strlen(message);
			res = f_write(&log->file, message, bTw, &bw);	// Write data to the file
			res = f_write(&log->file, CRLF, strlen(CRLF), &bw); // end the line
			// Flush the file buffer, don't close
			res = f_sync(&log->file);

			if(res != FR_OK){
				log->open = false;
				cardExist = false;
			}
		}

	}

return bw;
}

int nLogTypes() {

return nTypes;
}

int removeLogType(char* logName) {

// Loop control
int i, j;

// Iterate through array of logs
for (i = 0; i < nTypes; i++) {
	// find log in question
	if (!strcmp(logTypeList[i].logName, logName)) {
		// Remove logtype from array
		// Move all logtypes down the array
		for (j = i; j < nTypes; j++) {
			// End of array so make last value null
			if (j == (nTypes - 1)) {
				memset(&logTypeList[i].logName[0], NULL, MAX_PATH);
				memset(&logTypeList[i].path[0], NULL, MAX_NAME);
			}
			// Move later logtype down array
			else {
				strcpy(&logTypeList[j].logName[0],
						&logTypeList[j + 1].logName[0]);
				strcpy(&logTypeList[j].path[0], &logTypeList[j + 1].path[0]);
			}
		}
		// decrememnt number of Types
		nTypes--;
		// Sucessfully removed
		return 0;
	}
}
return -1;
}

logTypeS* getLogType(int index) {

return (logTypeList + index);
}

