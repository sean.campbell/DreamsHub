/*
 * Logging.h
 *
 *  Created on: 30 Jan 2016
 *      Author: David
 */

#ifndef LOG_DREAMS_H_
#define LOG_DREAMS_H_

#include "FatFs/ff.h"		// Fatfs library

#include <stdbool.h>

// Max number of logging types
#define MAX_N_LOGS 2
// Maximum path length
#define MAX_PATH 127
// Maximum file name length
#define MAX_NAME 16
// The New line marker
#define CRLF "\r\n\0"
// maximum size for a file (In bytes), has to be < 4Gb
#define MAX_FILE_SIZE 3145728

/* This struct contains all the relevent information about a
 * circular buffer/Queue.
 */
typedef struct  logTypeS
{
  char path[MAX_PATH]; // path where the logs should be written.
  int pathLen; // Length of path
  char logName[MAX_NAME]; // Name of the log type
  int nameLen; // Length of logName
  FIL file; // Representation of an open file in FatFs
  bool open;
} logTypeS;

/*
 * initialised peripheral communication for logging
 * returns:
 * 	-1 if SD interface is not initialised
 * 	-2 for file system error
 * 	-3 for name error
 * 	0 for success
 */
int log_init();

/*
 *	This function creates a new log type.
 * params:
 * 	path: The file path where the logs should be written
 * 	logName: Name of file that these logs should be written to,
 * 	should be null character terminated. This will become the name of log type.
 * 	updateLen: enum representing the length of time
 * 	each log file should cover
 * returns:
 * 	-1 for unsuccessful
 * 	+ve int for number of active logs
 */
int log_createNew(char* path, char* logName);

/*
 * Writes a log message to a specific log file at a specific log level.
 * params:
 * 	path:
 * 	logName: Name of Logtype, should be null character terminated
 * 	message:
 * returns:
 * 	-1 if SD interface is not initialised
 * 	-2 for file system error
 * 	-3 for file name error
 * 	+ve int for number of chars writen.
 */
int log_writeLine(char* logName, char * message);

/* Returns the number of logging types present within the system.
 * returns:
 * 	int representing number of logging types.
 */
int nLogTypes();


/* Get the pointer to a specific log type
 * params:
 * 	index: index to the  log type requried
 * returns:
 * 	logType struct with the information about a log type
 */
logTypeS* getLogType(int index);

#endif /* LOG_DREAMS_H_ */
