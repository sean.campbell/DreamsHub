/*
 * circularBuffer.h
 *
 */

#ifndef MISC_DREAMS_CIRCULARBUFFER_H_
#define MISC_DREAMS_CIRCULARBUFFER_H_

#include <stdbool.h>
#include <stdint.h>

#define BUFFER_SIZE 256
#if (BUFFER_SIZE & (BUFFER_SIZE - 1)) != 0
	#error "BUFF_SIZE must be a power of two"
#endif
#define BUFFER_MASK (BUFFER_SIZE-1)

typedef struct circularBuffer{
      char buffer[BUFFER_SIZE];
      uint_fast32_t head;
      uint_fast32_t tail;
} circularBuffer_t;

void circularBuffer_init(circularBuffer_t *buffer);

void circularBuffer_put(circularBuffer_t *buffer, const char data);
//void circularBuffer_putArray(circularBuffer_t *buffer, const unsigned char *data, uint_fast32_t numBytes);

bool circularBuffer_get(circularBuffer_t *buffer, char *data);
//uint_fast32_t circularBuffer_getArray(circularBuffer_t *buffer, unsigned char *data, uint_fast32_t maxBytes);

void circularBuffer_flush(circularBuffer_t *buffer, bool clear);

bool circularBuffer_isEmpty(circularBuffer_t *buffer);

bool circularBuffer_isFull(circularBuffer_t *buffer);

uint_fast32_t circularBuffer_size(circularBuffer_t *buffer);

#endif /* MISC_DREAMS_CIRCULARBUFFER_H_ */
