/*
 * circularBuffer.c
 *
 */
#include "circularBuffer.h"
#include <string.h> // For memset

void circularBuffer_init(circularBuffer_t *buffer){
	buffer->head = 0;
	buffer->tail = 0;
	memset(buffer->buffer, '\0', BUFFER_SIZE);
}

void circularBuffer_put(circularBuffer_t *buffer, const char data){
	if(circularBuffer_isFull(buffer)){
		buffer->tail = ((buffer->tail + 1) & BUFFER_MASK);
	}
	buffer->buffer[buffer->head] = data;
	buffer->head = ((buffer->head + 1) & BUFFER_MASK);
}

/*void circularBuffer_putArray(circularBuffer_t *buffer, const unsigned char *data, uint_fast32_t numBytes){
	uint_fast32_t idx = 0;
	for(; idx < numBytes; idx++){
		circularBuffer_put(buffer, *(data + idx));
	}
}*/

bool circularBuffer_get(circularBuffer_t *buffer, char *data){
	bool ret = false;
	if(!circularBuffer_isEmpty(buffer)){
		*data = buffer->buffer[buffer->tail];
		buffer->tail = ((buffer->tail + 1) & BUFFER_MASK);
		ret = true;
	}
	return ret;
}

/*uint_fast32_t circularBuffer_getArray(circularBuffer_t *buffer, unsigned char *data, uint_fast32_t maxBytes){
	uint_fast32_t ret = 0;
	if(!circularBuffer_isEmpty(buffer)){
		unsigned char *tmp = data;
		while(circularBuffer_get(buffer, tmp) && (ret < maxBytes)){
			ret++;
			tmp++;
		}
	}
	return ret;
}*/

void circularBuffer_flush(circularBuffer_t *buffer, bool clear){
	buffer->head = 0;
	buffer->tail = 0;
	if(clear){
		memset(buffer->buffer, '\0', BUFFER_SIZE);
	}
}

bool circularBuffer_isEmpty(circularBuffer_t *buffer) {
	return (buffer->head == buffer->tail);
}

bool circularBuffer_isFull(circularBuffer_t *buffer) {
	return ((buffer->head - buffer->tail) & BUFFER_MASK) == BUFFER_MASK;
}

uint_fast32_t circularBuffer_size(circularBuffer_t *buffer) {
	return ((buffer->head - buffer->tail) & BUFFER_MASK);
}
