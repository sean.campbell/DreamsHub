/*
 * hub.c
 *
 *  Created on: 11 Feb 2016
 *      Author: David
 */
#include "driverlib.h"

#include "adc_DREAMS.h"
#include "clocking_DREAMS.h"
#include "comms_DREAMS.h"
#include "DS1307.h"
#include "log_DREAMS.h"
#include "modem_DREAMS.h"

#include "scheduler.h"

#include "GenericCircBuff.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "hub_DREAMS.h"

// Changeable aspects
#define DREAMS_SERVER_HOSTNAME 	"dreams.3utilities.com"
#define DREAMS_SERVER_PORT		"2389"
#define HUB_SITE_ID				1

/********************** Defines for pinout to HUB PCB *****************************/
// LED 0
#define LED0_PORT GPIO_PORT_P9
#define LED0_PIN GPIO_PIN3
// LED 1
#define LED1_PORT GPIO_PORT_P6
#define LED1_PIN GPIO_PIN3
// LED 2
#define LED2_PORT GPIO_PORT_P7
#define LED2_PIN GPIO_PIN2

/********************** Defines for Log info *****************************/
#define MAX_RES_LEN 	10
#define LOG_RATE 		10
#define LOG_1_NAME 		"sensors"
#define LOG_1_PATH 		"/testRun"

/********************** Defines for Data Logging *****************************/
#define DATA_BUFFER_SIZE		2048

// Message to log to SD and Server
#define MESSAGE_START 			"{\"SiteID\":%d, \"Readings\":["
#define MESSAGE_TIMESTAMP		"%04d-%02d-%02dT%02d:%02d:%02d"
#define MESSAGE_READING			"{\"TimeStamp\":\"%s\", \"sensor%02d\":%.5f}"
#define MESSAGE_END				"]}"

// 44 assumes 5 decimal points + several preceding
#define MAX_LENGTH 				DATA_BUFFER_SIZE - (20 + 44 + 3)

/********************** Defines for Scheduler Task IDs *****************************/
// Use adcSettings->sensorId for readAdc functions
#define TASK_LOG_ID				500
#define TASK_LED_ID				501

// Global variable for Comms; eliminates additional param passing
uint_fast32_t HUB_ID = HUB_SITE_ID;

// Private
static circBuffS readingBuff;
static char data[DATA_BUFFER_SIZE], time[22], temp[128];

// Oscillating emergency SMS messages
static void checkThresholds(adcSensorSettings *settings, float value){
	bool toSend = false;
	float threshold = 0.0;
	if(!settings->belowLowerThreshold && value < settings->lowerThreshold){
		threshold = settings->lowerThreshold;

		settings->belowLowerThreshold = true;
		settings->aboveUpperThreshold = false;
		toSend = true;
	}
	else if(!settings->aboveUpperThreshold && value > settings->upperThreshold){
		threshold = settings->upperThreshold;

		settings->aboveUpperThreshold = true;
		settings->belowLowerThreshold = false;
		toSend = true;
	}	// Reset the flags
	else if(value > settings->lowerThreshold && value < settings->upperThreshold){
		settings->aboveUpperThreshold = false;
		settings->belowLowerThreshold = false;
	}

	// Call comms function to send message
	if(toSend){
		comms_sendEmergencySMS(settings->sensorID, threshold, value);
	}
}

void readADC(int sensorId) {
	scheduler_stop();
	adcSensorSettings *settings = NULL;
	// Get settings and if valid, ensure enabled
	if((settings = adc_getSensorSettings(sensorId)) != NULL && settings->sensorEn){
		// Read the sensor value
		float result = adc_readSensor(sensorId);

		// Fetch the time
		timeS currTime;
		getTime(&currTime);

		// Build the buffer entry
		union buffU bufferEntry;
		readingS reading = {settings->sensorID, currTime, result};
		bufferEntry.reading = reading;
		circBuffS_put(&readingBuff, bufferEntry);

		// Check if emergency situation
		checkThresholds(settings, result);
	}
	scheduler_start();
}

void logToSD(char *data){
	int bw = log_writeLine(LOG_1_NAME, data);
	if(bw > 0){
		toggleLED2(0);
	}
}

void logToServer(char *data){
	toggleLED1(0);
	if(modem_openTCPConnection(DREAMS_SERVER_HOSTNAME, DREAMS_SERVER_PORT)){
		comms_sendReceiveMessage(data, DATA_BUFFER_SIZE);
	}
	toggleLED1(0);
}

void log(int arg1){
	scheduler_stop();

	snprintf(data, DATA_BUFFER_SIZE, MESSAGE_START, HUB_SITE_ID);
	uint_fast32_t numBytes = strlen(data);

	while(!circBuffS_empty(&readingBuff) && numBytes < MAX_LENGTH){
		readingS val = circBuffS_get(&readingBuff).reading;

		// ISO time YYYY-MM-DDTHH:mm:ss
		snprintf(time, sizeof(time), MESSAGE_TIMESTAMP,
			val.time.yr,
			val.time.mth,
			val.time.day,
			val.time.hr,
			val.time.min,
			val.time.sec);

		snprintf(temp, sizeof(temp), MESSAGE_READING,
			time,
			val.sensorID,
			val.value);

		if(!circBuffS_empty(&readingBuff)) strcat(temp, ",");
		strcat(data, temp);
		numBytes += strlen(temp);
	}

	strcat(data, MESSAGE_END);

	logToSD(&data[0]);
	logToServer(&data[0]);

	scheduler_start();
}

void initADC() {
	// Call relevant init
	adc_init();

	// Iterate through the settings and set settings if enabled
	uint_fast32_t idx = 0;
	adcSensorSettings *settings = NULL;
	while((settings = adc_getSensorSettings(idx)) != NULL){
		if(settings->sensorEn){
			scheduler_addTask(readADC, settings->sensorID, settings->sampleRate, settings->sensorID);
		}
		idx++;
	}
}

// Calls from comms layer
bool hub_processADCUpdate(adcSensorSettings *settings){
	bool present = scheduler_containsTask(settings->sensorID), ret = false;
	if(present && settings->sensorEn){
		ret = scheduler_editTaskPeriod(settings->sensorID, settings->sampleRate);
	}
	else if(present){
		ret = scheduler_deleteTask(settings->sensorID);
	}
	else if(settings->sensorEn){
		ret = scheduler_addTask(readADC, settings->sensorID, settings->sampleRate, settings->sensorID);
		adc_initOne(settings->sensorID);
	}
	else{
		ret = true;
	}

	return ret;
}

static void parseShortField(char *strtokArg, char *delims, uint16_t *ptr){
	char *temp = strtok(strtokArg, delims);
	if(temp != NULL){
		*ptr = atoi(temp);
	}
}

static void parseCharField(char *strtokArg, char *delims, uint8_t *ptr){
	char *temp = strtok(strtokArg, delims);
	if(temp != NULL){
		*ptr = atoi(temp);
	}
}

// ISO Time-stamp W-YYYY-MM-DDTHH:MM:SS	(Skip whitespace)
#define ISO_TIMESTAMP_DELIMS	"-:T"
bool hub_processTimeStampUpdate(char *isoTimeStamp){
	int i = 0;
	timeS tm;
	parseCharField(isoTimeStamp, ISO_TIMESTAMP_DELIMS, &tm.wDay);
	parseShortField(NULL, ISO_TIMESTAMP_DELIMS, &tm.yr);
	parseCharField(NULL, ISO_TIMESTAMP_DELIMS, &tm.mth);
	parseCharField(NULL, ISO_TIMESTAMP_DELIMS, &tm.day);
	parseCharField(NULL, ISO_TIMESTAMP_DELIMS, &tm.hr);
	parseCharField(NULL, ISO_TIMESTAMP_DELIMS, &tm.min);
	parseCharField(NULL, ISO_TIMESTAMP_DELIMS, &tm.sec);

	for(i = 0; i < 3;i++){
		if(setTime(&tm)) break;
	}

	return true;
}

void toggleLED0(int a) {
	MAP_GPIO_toggleOutputOnPin(LED0_PORT, LED0_PIN);
}
void toggleLED1(int a) {
	MAP_GPIO_toggleOutputOnPin(LED1_PORT, LED1_PIN);
}
void toggleLED2(int a) {
	MAP_GPIO_toggleOutputOnPin(LED2_PORT, LED2_PIN);
}

void initLEDs() {
	MAP_GPIO_setAsOutputPin(LED0_PORT, LED0_PIN);
	MAP_GPIO_setAsOutputPin(LED1_PORT, LED1_PIN);
	MAP_GPIO_setAsOutputPin(LED2_PORT, LED2_PIN);

	scheduler_addTask(toggleLED0, TASK_LED_ID, 2, 0);
}

void initModem() {
	modem_powerOn();
	// TODO
	//modem_clearSIM();
	//modem_addContact("00447752464352", "Sean", false);

	// Ensure the modem is off the network
	modem_closeTCPConnection(true);

	// Try several times to connect to the server on first power up
	bool connected = false;
	uint_fast8_t retry = 0;
	while(retry < 3 && !connected){
		connected = modem_openTCPConnection(DREAMS_SERVER_HOSTNAME, DREAMS_SERVER_PORT);
		timer32_delay(100);
		retry++;
	}

	// If connected, perform the startup protocol
	if(connected){
		comms_setupProtocol(data, DATA_BUFFER_SIZE);
	}
}

void initHub(){
    /* Enabling the FPU for floating point operation */
    MAP_FPU_enableModule();
    MAP_FPU_enableLazyStacking();

	circBuffS_init(&readingBuff);

	setupClocks();

	initRTC();

	initADC();

	initLEDs();

	// Initialise SD card and logging capabilities
	int res = log_init();
	if(res) log_createNew(LOG_1_PATH, LOG_1_NAME);
	scheduler_addTask(log, TASK_LOG_ID, LOG_RATE, 0);

	initModem();

	scheduler_start();
}
