/*
 * hub.h
 *
 *  Created on: 10 Feb 2016
 *      Author: David
 */

#ifndef LIB_HUB_H_
#define LIB_HUB_H_

#include "adc_DREAMS.h"
#include "DS1307.h"	// For timeS struct

#include <stdint.h>

typedef struct readingS {
	uint8_t sensorID;
	timeS time;
	float value;
} readingS;

void initHub();

// Calls from comms layer
bool hub_processADCUpdate(adcSensorSettings *settings);
bool hub_processTimeStampUpdate(char *isoTimeStamp);

// Tasks for scheduler
void readSensor(int sensorId);
void logData(int arg1);

void toggleLED0(int a);
void toggleLED1(int a);
void toggleLED2(int a);

#endif /* LIB_HUB_H_ */
