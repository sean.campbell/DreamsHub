/*
 * CircBuff.c
 *
 *  Created on: 27 Nov 2015
 *      Author: David
 */

#include "GenericCircBuff.h"
#include <string.h>

void circBuffS_init (circBuffS *_this)
{
	_this->count  = 0;
	_this->head   = 0;
	_this->tail   = 0;
	memset (_this, 0, sizeof (*_this));
}


int circBuffS_empty (circBuffS *_this)
{
   return (0==_this->count);
}

int circBuffS_full (circBuffS *_this)
{
    return (_this->count>=BUFF_SIZE);
}

union buffU circBuffS_get (circBuffS *_this)
{
    union buffU c;
    if (_this->count>0)
    {
      c = _this->buf[_this->tail];
      _this->tail =  (++_this->tail % BUFF_SIZE);
      --_this->count;
    }
    else
    {
      c.uChar = 0;
    }
    return (c);
}

void circBuffS_put (circBuffS *_this, union buffU c)
{
    if (_this->count < BUFF_SIZE)
    {
      _this->buf[_this->head] = c;
      _this->head = (++_this->head % BUFF_SIZE);
      ++_this->count;
    }
}

void  circBuffS_flush  (circBuffS *_this, const int clear){
	_this->count = 0;
	_this->head = 0;
	_this->tail = 0;

	if(clear){
		memset (_this, 0, sizeof (*_this));
	}
}



