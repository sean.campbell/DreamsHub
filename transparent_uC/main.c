/* DriverLib Includes */
#include "driverlib.h"

/* Standard Includes */
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

/* Home Grown Includes*/
#include "CircBuff.h"

/* UART Configuration Parameter. These are the configuration parameters to
 * make the eUSCI A UART module to operate with a 9600 baud rate. These
 * values were calculated using the online calculator that TI provides
 * at:
 *http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSP430BaudRateConverter/index.html
 */
const eUSCI_UART_Config ModemUartConfig =
{
        EUSCI_A_UART_CLOCKSOURCE_SMCLK,          // SMCLK Clock Source
        78,                                     // BRDIV = 78
        2,                                       // UCxBRF = 2
        0,                                       // UCxBRS = 0
        EUSCI_A_UART_NO_PARITY,                  // No Parity
        EUSCI_A_UART_LSB_FIRST,                  // MSB First
        EUSCI_A_UART_ONE_STOP_BIT,               // One stop bit
        EUSCI_A_UART_MODE,                       // UART mode
        EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION  // Oversampling
};

/* UART Configuration Parameter. These are the configuration parameters to
 * make the eUSCI A UART module to operate with a 115200 baud rate. These
 * values were calculated using the online calculator that TI provides
 * at:
 *http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSP430BaudRateConverter/index.html
 */
const eUSCI_UART_Config PCUartConfig = {
		EUSCI_A_UART_CLOCKSOURCE_SMCLK,          // SMCLK Clock Source
		6,                                      // BRDIV = 26
		8,                                       // UCxBRF = 0
		0,                                       // UCxBRS = 0
		EUSCI_A_UART_NO_PARITY,                  // No Parity
		EUSCI_A_UART_LSB_FIRST,                  // MSB First
		EUSCI_A_UART_ONE_STOP_BIT,               // One stop bit
		EUSCI_A_UART_MODE,                       // UART mode
		EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION  // Low Frequency Mode
		};

// Buffer to store incoming data.
struct circBuffS rxBuff;

// Transmit constants
char AT[] = "AT";
char atc1[] = "AT+CMGF=1";                 // Command for setting SMS text mode
char atc2[] = "AT+CMGD=1,4";               // Command for erasing all messages from inbox
char atc3[] = "AT+CMGR=1";                 // Command for reading message from location 1 from inbox

// Recieve Constants
char OK[] ="OK";
/*
 * Hardware delay using Timer32 peripheral
 *
 * Input number of ms to wait for, max value is (2^32)/128
 */
void delay(int ms){

	// Clocked at 12MHz so 1 ms is 12000 cycles
	int totalTime = 12000*ms;

	// Set number of cycles to count to
    MAP_Timer32_setCount(TIMER32_0_MODULE, totalTime);

    // start timer
    MAP_Timer32_startTimer(TIMER32_0_MODULE, true);

    // wait until delay has passed
    while(!Timer32_getInterruptStatus(TIMER32_0_MODULE));

    // clear flag
    MAP_Timer32_clearInterruptFlag(TIMER32_0_MODULE);


}

void sendAT(char *txData, unsigned char txLength){

	while(txLength){

	// Wait for TX buffer to be ready for new data
	while(!(UCA2IFG & UCTXIFG));

		// Push data to TX buffer
		MAP_UART_transmitData(EUSCI_A2_MODULE, *txData);

		// Update indexes
		txLength--;
		txData++;
	}

	// Transmit carraige return char,
	// all AT commands need to be terminated by this
	MAP_UART_transmitData(EUSCI_A2_MODULE, 0x0D);

	// Wait until the last byte is completely sent
	while(UCA2STATW & UCBUSY);
}


/*
 * Helper function to set up the UART
 * Just setting up UART 3 for now,
 * TODO Generalise this.
 */
void setup_UART(void){

	/***************************UARTA2 to Modem***************************/
    /* Selecting P9.6 and P9.7 in UART mode */
    MAP_GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P3,
            GPIO_PIN2 | GPIO_PIN3, GPIO_PRIMARY_MODULE_FUNCTION);

    /* Setting DCO to 12MHz */
    MAP_CS_setDCOCenteredFrequency(CS_DCO_FREQUENCY_12);

    /* Configuring UART Module */
    MAP_UART_initModule(EUSCI_A2_MODULE, &ModemUartConfig);

    /* Enable UART module */
    MAP_UART_enableModule(EUSCI_A2_MODULE);

    /* Enabling interrupts */
    MAP_UART_enableInterrupt(EUSCI_A2_MODULE, EUSCI_A_UART_RECEIVE_INTERRUPT);
    MAP_Interrupt_enableInterrupt(INT_EUSCIA2);


	/***************************UARTA0 to PC***************************/
	/* Selecting P1.2 and P1.3 in UART mode. */
	MAP_GPIO_setAsPeripheralModuleFunctionInputPin(GPIO_PORT_P1,
	GPIO_PIN2 | GPIO_PIN3, GPIO_PRIMARY_MODULE_FUNCTION);

	/* Configuring UART Module */
	MAP_UART_initModule(EUSCI_A0_MODULE, &PCUartConfig);

	/* Enable UART module */
	MAP_UART_enableModule(EUSCI_A0_MODULE);

	// Enabnle interrupts
	UART_enableInterrupt(EUSCI_A0_MODULE, EUSCI_A_UART_RECEIVE_INTERRUPT);
	Interrupt_enableInterrupt(INT_EUSCIA0);

	// Enable master interrupts
    MAP_Interrupt_enableMaster();

}

/*
 * Helper Function, checks buffer for a char sequence. It will burn through the buffer
 * until it finds expected sequence.
 */
int checkBuffer(circBuffS *buff, char *expected, int len){

	int i =0;

	// Do until buffer is empty
	while(!circBuffS_empty(buff)){

		if(expected[i] == circBuffS_get(buff)){
			i++;
		}
		else i = 0;
		// If we have found match, return 1
		if(i == len) return 1;
	}
	return -1;

}

/*
 * Helper function to set timer up as required for delay function
 * This uses the Timer32 peripheral, it is clocked at 12Mhz from the DCO.
 * TODO, get this to use a different clock source.
 */
void initDelay(void){

    /* Configuring Timer32 to use SMCLK (12MHz) in periodic mode */
    MAP_Timer32_initModule(TIMER32_0_MODULE, TIMER32_PRESCALER_1, TIMER32_32BIT,
            TIMER32_PERIODIC_MODE);
}
/*
 * Helper function which will power the modem up,
 * Should only be called  after UART has been set up.
 */
int init_Modem(void){

	// Set reset pin to output and low
	GPIO_setAsOutputPin(GPIO_PORT_P2, GPIO_PIN4);
	GPIO_setOutputLowOnPin(GPIO_PORT_P2, GPIO_PIN4);

	// Set status pin as input
	GPIO_setAsInputPin(GPIO_PORT_P5, GPIO_PIN6);

	// Set power pin output and high
	GPIO_setAsOutputPin(GPIO_PORT_P6, GPIO_PIN6);


	// Hold power key down for 2 seconds
	GPIO_setOutputLowOnPin(GPIO_PORT_P6, GPIO_PIN6);
	delay(2000);

	// Pull power key up, then wait for poweron
	GPIO_setOutputHighOnPin(GPIO_PORT_P6, GPIO_PIN6);
	delay(3000);

	// Wait until Modem reports it's status as okay
	while(GPIO_getInputPinValue(GPIO_PORT_P5, GPIO_PIN6) == 0){}

    while(1){
		// Send Data
		sendAT(AT, (sizeof(AT)/sizeof(AT[0]))-1);
		delay(200); // Wait 200ms

		// Wait for OK
		if(checkBuffer(&rxBuff, OK, 2)==1) break;
		delay(1000);// Wait 1 seconds and retry
    }

	return 1;
}


int main(void){
    // Halt WDT
    MAP_WDT_A_holdTimer();

	// Initialise the reception buffer
	circBuffS_init(&rxBuff);

    // Setup LED1 and put off
    GPIO_setAsOutputPin(GPIO_PORT_P1,GPIO_PIN0);
    GPIO_setOutputLowOnPin(GPIO_PORT_P1,GPIO_PIN0);

    // Set up timers
    initDelay();

    printf("Initialising UARTs.\n\r");
    // Set up UART peripheral
    setup_UART();

    printf("Initialising Modem.\n\r");
    // Setup modem
    init_Modem();


    delay(1000); // wait 1 second


    printf("Any text entered here should be passed through to the Modem.\n\r");

    circBuffS_flush(&rxBuff, 1); // Clear the buffer
    GPIO_setOutputHighOnPin(GPIO_PORT_P1,GPIO_PIN0);

    for(;;);

}


/* EUSCI A2 UART ISR - Echoes data back to PC host */
void euscia2_isr(void){

	uint8_t rcvd = MAP_UART_receiveData(EUSCI_A2_MODULE);
	// Recieve byte and put into buffer
	circBuffS_put(&rxBuff, rcvd);

	if (rcvd == 0x0D){
		MAP_UART_transmitData(EUSCI_A0_MODULE, '\n');
		MAP_UART_transmitData(EUSCI_A0_MODULE, '\r');
	}
	else{
		MAP_UART_transmitData(EUSCI_A0_MODULE, rcvd);
	}

	// Clear interupts
    uint32_t status = MAP_UART_getEnabledInterruptStatus(EUSCI_A2_MODULE);
    MAP_UART_clearInterruptFlag(EUSCI_A2_MODULE, status);
}

void euscia0_isr(void){

	uint8_t rcvd = MAP_UART_receiveData(EUSCI_A0_MODULE);

	// See if a newline or escape character was received.
	if (rcvd == '\r'){
		MAP_UART_transmitData(EUSCI_A0_MODULE, '\n');
		MAP_UART_transmitData(EUSCI_A0_MODULE, '\r');
		MAP_UART_transmitData(EUSCI_A2_MODULE, 0x0D);
	}
	else{
		MAP_UART_transmitData(EUSCI_A0_MODULE, rcvd);
		MAP_UART_transmitData(EUSCI_A2_MODULE, rcvd);
	}
	// Clear interupts
    uint32_t status = MAP_UART_getEnabledInterruptStatus(EUSCI_A0_MODULE);
    MAP_UART_clearInterruptFlag(EUSCI_A0_MODULE, status);
}

