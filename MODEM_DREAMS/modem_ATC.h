/*
 * modem_ATC.h
 *
 *  Created on: 28 Jan 2016
 *      Author: Sean
 */

#ifndef MODEM_ATC_H_
#define MODEM_ATC_H_

#include <stdbool.h>
#include <stdint.h>

typedef struct {
	// General reading and writing functionality
	void (*writeByteFunc)(const char data);		// Write to the appropriate buffer

	bool (*readByteFunc)(char *data);			// Attempt to read a byte
	uint_fast32_t (*numBytesReadable)(void);	// Query the number of bytes available in the buffer

	void (*delayFunc)(const uint32_t delay);	// A delay to associate with writeEndDelayFunc + readByteFunc if response not ready

	// Error buffering and error logging
	void (*writeErrorFunc)(const char *data, const uint_fast32_t len);	// Write data to the appropriate error buffer

	void (*logger)(const char *data, const uint_fast32_t len); // Write the the appropriate error log
} AT_UART_Config;

// TCP Connection status query
typedef enum{
	IP_INITIAL = 0,
	IP_START,
	IP_CONFIG,
	IP_GPRSACT,
	IP_STATUS,
	TCP_CONNECTING,
	UDP_CONNECTING,
	CONNECT_OK,
	TCP_CLOSING,
	UDP_CLOSING,
	TCP_CLOSED,
	UDP_CLOSED,
	PDP_DEACT,
	IP_ERROR
} IP_Connect_Status;


// Basic commands
bool at_checkReady(const AT_UART_Config *config);
void at_setEchoMode(const AT_UART_Config *config, bool setOnEcho);
void at_powerOff(const AT_UART_Config *config);
int_fast16_t at_getCredit(const AT_UART_Config *config, const char *number);

// Phonebook commands
bool at_addContact(const AT_UART_Config *config, const char *number, const char *id, bool nationalNumber);
bool at_removeContact(const AT_UART_Config *config, const char *id);
void at_clearSIMContacts(const AT_UART_Config *config);

// SMS commands
void at_selectSMSFormat(const AT_UART_Config *config, bool textMode);
bool at_sendSMS(const AT_UART_Config *config, const char *number, const char *message);
bool at_sendBlanketSMS(const AT_UART_Config *config, const char *message);
void at_deleteAllSMS(const AT_UART_Config *config);

// TCP-IP commands
IP_Connect_Status at_queryConnectionStatus(const AT_UART_Config *config);

// Necessary for establishing a connection
bool at_checkGPRSServiceStatus(const AT_UART_Config *config);
void at_setupGPRSConnection(const AT_UART_Config *config, bool attach);

bool at_setAPN(const AT_UART_Config *config, const char *apn, const char *usr,
		const char *pwd);
bool at_bringupWirelessConnection(const AT_UART_Config *config);
bool at_obtainIPAddress(const AT_UART_Config *config, char *ipAddress);

bool at_establishTCPConnection(const AT_UART_Config *config, const char *ipAddress, const char *port);
bool at_sendTCPMessage(const AT_UART_Config *config, const char *message);
bool at_closeTCPConnection(const AT_UART_Config *config);
bool at_deactivatePDPContext(const AT_UART_Config *config);

#endif /* MODEM_ATC_H_ */
