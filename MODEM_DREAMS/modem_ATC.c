/*
 * modem_ATC.c
 *
 *  Created on: 30 Jan 2016
 *      Author: Sean
 */

#include "modem_ATC.h"

#include <stdio.h>	// For logging
#include <stdarg.h> // For variadic logging func
#include <stdlib.h>
#include <string.h>

// Precaution
#ifndef NULL
#define NULL ((void*)0)
#endif

#define strlens(s) (s == NULL ? 0 : strlen(s))

#define MAX_RESPONSE_SIZE 	128
#define LOG_BUFFER_SIZE 	128
#define TCP_BUFFER_SIZE		256

#define DEFAULT_RETRIES 3
#define MAX_RETRIES 5
#define RETRY_DELAY 250

// Modes for reference
/*
#define at_BASIC 			"AT&<x><n>" // x denotes command, n denotes variadic parameters
#define at_S 				"ATS<n><m>" // n denotes index of S reg, m denotes value to assign
#define at_TEST				"AT+<x>=?" // x denotes command; returns list of params + values relevant to write
#define at_READ				"AT+<x>?" // x denotes command; returns currently set value of param or params
#define at_WRITE			"AT+<x>=<...>" // x denotes command; sets the defined parameter values
#define at_EXEC				"AT+<x>"  // Reads non-variable params affect by internal process in GSM engine
*/

// Typical responses
#define at_OK			"OK"
#define at_ERROR		"ERROR"
#define at_RESPONSE_DELIMS	":,"

// Interaction modes
#define at_BASIC			"AT"
#define at_EXTENDED	at_BASIC "+"

#define at_TEST(val)			at_EXTENDED val "=?"
#define at_READ(val)			at_EXTENDED val "?"
#define at_WRITE(par, val)		at_EXTENDED par "=" val
#define at_EXEC(val)			at_EXTENDED val

// V.25TER AT Commands
// Start-up
#define at_CHK			"AT"
// Repeat last command
#define atc_REPEAT		"A/"
// Echo mode
#define atc_ECHO		"ATE"
#define atw_ECHO(on)	(const char *)((on == true) ?  "1" : atc_ECHO "0")
// Dial a number
#define atc_DIAL		"ATD"

#define c_SELECTPHONEBOOKSTORAGE	"CPBS"
#define atr_SELECTPHONEBOOKSTORAGE	at_READ(c_SELECTPHONEBOOKSTORAGE)
#define atr_SELECTPHONEBOOKSTORAGE_USED_IDX 	2
#define atr_SELECTPHONEBOOKSTORAGE_TOTAL_IDX 	3

#define c_FINDPHONEBOOKENTRY		"CPBF"
#define c_FINDPHONEBOOKENTRY_ALT	"CBPF"
#define atw_FINDPHONEBOOKENTRY		at_WRITE(c_FINDPHONEBOOKENTRY, "")
#define atw_FINDPHONEBOOKENTRY_IDX	1

#define c_WRITEPHONEBOOKENTRY		"CPBW"
#define atw_WRITEPHONEBOOKENTRY		at_WRITE(c_WRITEPHONEBOOKENTRY, "")
#define c_ISNATIONALNUMBER(num)		((num == true) ? "129" : "145")

#define c_READPHONEBOOK				"CPBR"
#define atw_READPHONEBOOK			at_WRITE(c_READPHONEBOOK, "1,")

// AT Commands according to 3GPP TS 27.005
#define c_DELETESMS					"CMGD"

#define c_SELECTSMSFMT				"CMGF"
#define at_SELECTSMSFMT_TEXT(val)	(val == true) ? "+" c_SELECTSMSFMT ":1" : "+" c_SELECTSMSFMT ":0"

#define att_SELECTSMSFMT_ISTEXT		at_READ(c_SELECTSMSFMT)
#define att_SELECTSMSFMT_ISTEXT_RSP	"+" c_SELECTSMSFMT ": 1"
#define atw_SELECTSMSFMT(textMode)	(const char *)((textMode == true) ?		\
											at_WRITE(c_SELECTSMSFMT, "1") : at_WRITE(c_SELECTSMSFMT, "0"))

//#define c_LISTSMS					"+CMGL"

#define c_READSMS					"CMGR"
//#define atc_READSMS				at_EXTENDED(c_READSMS)

#define c_SENDSMS					"CMGS"
#define atw_SENDSMS					at_WRITE(c_SENDSMS, "")
#define at_ENTERTEXT				"> "
#define at_ESCAPETEXT				"\032"

// TCP Commands
#define c_CHECKCONNECTIONSTATUS		"CIPSTATUS"
#define ate_CHECKCONNECTIONSTATUS	at_EXEC(c_CHECKCONNECTIONSTATUS)
#define ate_CHECKCONNECTIONSTATUS_RSP "STATE: "


#define c_ATTACHGPRS				"CGATT"
#define atr_ATTACHGPRS				at_READ(c_ATTACHGPRS)
#define atr_ATTACHGPRS_RSP			"+" c_ATTACHGPRS ": 1"
#define atw_ATTACHGPRS(attach)		(const char*)((attach == true) ? 			\
										at_WRITE(c_ATTACHGPRS, "1") : at_WRITE(c_ATTACHGPRS, "0"))

#define c_SETAPN					"CSTT"
#define atw_SETAPN					at_WRITE(c_SETAPN, "")

#define c_BRINGUPWIRELESS			"CIICR"
#define ate_BRINGUPWIRELESS			at_EXEC(c_BRINGUPWIRELESS)

#define c_GETLOCALIP				"CIFSR"
#define ate_GETLOCALIP				at_EXEC(c_GETLOCALIP)

#define c_STARTIPCONNECTION			"CIPSTART"
#define atw_STARTTCPCONNECTION		at_WRITE(c_STARTIPCONNECTION, "\"TCP\",")
#define atw_STARTUDPCONNECTION		at_WRITE(c_STARTIPCONNECTION, "\"UDP\",")
#define atw_STARTIPCONNECTION_RSP	"CONNECT OK"

#define c_SENDIPMESSAGE				"CIPSEND"
#define ate_SENDIPMESSAGE			at_EXEC(c_SENDIPMESSAGE)
#define ate_SENDIPMESSAGE_RSP		"> "
#define ate_SENDIPMESSAGE_RSP2		"SEND OK"

#define c_CLOSEIPCONNECTION			"CIPCLOSE"
#define ate_CLOSEIPCONNECTION		at_EXEC(c_CLOSEIPCONNECTION)
#define ate_CLOSEIPCONNECTION_RSP	"CLOSE OK"

#define c_DEACTIVATEPDPCONTEXT		"CIPSHUT"
#define ate_DEACTIVATEPDPCONTEXT	at_EXEC(c_DEACTIVATEPDPCONTEXT)
#define ate_DEACTIVATEPDPCONTEXT_RSP "SHUT OK"
#define ate_DEACTIVATEPDPCONTEXT_RSP2 "+PDP:DEACT"
// AT Commands Special for SIMCom
#define c_POWERDOWN					"CPOWD"
#define atw_POWERDOWN_NORMAL		at_WRITE(c_POWERDOWN, "1")
#define atw_POWERDOWN_RSP			"NORMAL POWER DOWN"

#define c_DELETEALLSMS				"CMGDA"
#define atw_DELETEALLSMS_PDU		at_WRITE(c_DELETEALLSMS, "6")
#define atw_DELETEALLSMS_TXT		at_WRITE(c_DELETEALLSMS, "DEL ALL")


// Cellular unstructured data request, i.e. for credit
#define c_UNSTRUCTUREDDATA			"CUSD"
#define atw_UNSTRUCTUREDDATA		at_WRITE(c_UNSTRUCTUREDDATA, "1,")
#define atw_UNSTRUCTUREDDATA_DCS	"15"
#define atw_UNSTRUCTUREDDATA_RSP_DELIM	'�'

// Mapping structs
// Indexing dependency with enum declarations
static const char *ipConnectionStatuses[] = {
		ate_CHECKCONNECTIONSTATUS_RSP "IP INITIAL",
		ate_CHECKCONNECTIONSTATUS_RSP "IP START",
		ate_CHECKCONNECTIONSTATUS_RSP "IP CONFIG",
		ate_CHECKCONNECTIONSTATUS_RSP "IP GPRSACT",
		ate_CHECKCONNECTIONSTATUS_RSP "IP STATUS",
		ate_CHECKCONNECTIONSTATUS_RSP "TCP CONNECTING",
		ate_CHECKCONNECTIONSTATUS_RSP "UDP CONNECTING",
		ate_CHECKCONNECTIONSTATUS_RSP "CONNECT OK",
		ate_CHECKCONNECTIONSTATUS_RSP "TCP CLOSING",
		ate_CHECKCONNECTIONSTATUS_RSP "UDP CLOSING",
		ate_CHECKCONNECTIONSTATUS_RSP "TCP CLOSED",
		ate_CHECKCONNECTIONSTATUS_RSP "UDP CLOSED",
		ate_CHECKCONNECTIONSTATUS_RSP "PDP DEACT"
};


// Private Functions
static inline void delayFunction(const AT_UART_Config *config, uint32_t msDelay){
	if(config->delayFunc != NULL && msDelay > 0){
		config->delayFunc(msDelay);
	}
}

static void writeBytes(const AT_UART_Config *config, const char *data, uint32_t len, uint32_t msDelay, bool writeEnd){
	uint_fast32_t idx = 0;
	if(config->writeByteFunc != NULL){
		for(idx = 0; idx != len; idx++){
			config->writeByteFunc(data[idx]);
		}
		if(writeEnd){
			config->writeByteFunc(0x0D);
		}
		delayFunction(config, msDelay);
	}
}

static uint32_t readBytes(const AT_UART_Config *config, char *buffer, uint32_t maxBytes,
							uint32_t numRetries, uint32_t retryDelay){
	// Quick error check
	if(config->readByteFunc == NULL){
		return 0;
	}

	bool sawCR = false, gotChar = false;
	char readByte;
	uint_fast32_t idx = 0, retries = 0;

	while(idx < maxBytes && (retries != numRetries + 1)){
		gotChar = config->readByteFunc(&readByte);
		if(!gotChar){
			delayFunction(config, retryDelay);
			retries++;
		}
		else if(readByte != 0x0D && readByte != 0x0A){
			buffer[idx++] = readByte;
		}

		if(idx > 1){
			if(readByte == 0x0D){
				sawCR = true;
			}
			else if(sawCR && readByte == 0x0A){
				break;
			}
		}
	}
	return idx;
}

static bool checkResponse(const AT_UART_Config *config, const char *response, size_t resLen,
						const char *expected, size_t expLen){
	bool ret = false;
	if(resLen == expLen && !strncmp(response, expected, resLen)){
		ret = true;
	}
	return ret;
}

static void logError(const AT_UART_Config *config, const char *formatString, ...){
	if(config->logger != NULL){
		char errorBuffer[LOG_BUFFER_SIZE];
		int_fast32_t ret = 0;
		va_list args;
		va_start(args, formatString);
		ret = vsnprintf(errorBuffer, LOG_BUFFER_SIZE, formatString, args);
		va_end(args);

		if(ret > LOG_BUFFER_SIZE){
			ret = LOG_BUFFER_SIZE;
		}
		else if(ret == -1){
			ret = 0;
		}
		config->logger(errorBuffer, ret);
	}
}

static uint_fast32_t countAvailableBytes(const AT_UART_Config *config){
	uint_fast32_t ret = 0;
	if(config->numBytesReadable != NULL){
		ret = config->numBytesReadable();
	}
	return ret;
}

static void bufferError(const AT_UART_Config *config, const char *response, size_t resLen){
	if(config->writeErrorFunc != NULL){
		config->writeErrorFunc(response, resLen);
	}
}

static char *sendCommand_getResponse(const AT_UART_Config *config, const char *command,
		const uint32_t msDelay){
	char response[MAX_RESPONSE_SIZE], *ret = NULL;
	size_t numBytes = 0;

	writeBytes(config, command, strlens((const char*)command), msDelay, true);
	// TODO Architecture similar to the burn function?
	numBytes = readBytes(config, response, MAX_RESPONSE_SIZE, DEFAULT_RETRIES, msDelay);

	if(numBytes != 0){
		ret = malloc((numBytes + 1) * sizeof(char));
		if(ret != NULL){
			memcpy(ret, response, numBytes*sizeof(char));
			ret[numBytes * sizeof(char)] = '\0';
		}
	}
	return ret;
}

static uint_fast16_t sendCommand_Burn(const AT_UART_Config *config, const char *command, const char *expResponse,
		const uint32_t msDelay, const char *tag){
	char response[MAX_RESPONSE_SIZE];
	bool isCorrect = false;
	uint_fast32_t numBytes = 0, ret = 0;
	size_t expLen = strlens((const char*)expResponse);

	writeBytes(config, command, strlens((const char*)command), msDelay, true);
	do{
		numBytes = readBytes(config, response, MAX_RESPONSE_SIZE, MAX_RETRIES, msDelay);
		isCorrect = checkResponse(config, response, numBytes, expResponse, expLen);
		if(!isCorrect){
			logError(config, "%s: Expected %d bytes but received %d bytes", tag, expLen, numBytes);
			ret++;
			if(numBytes != 0){
				logError(config, "%s: Expected %.*s but received %.*s\n", tag, expLen, expResponse, numBytes, response);
				bufferError(config, response, numBytes);
			}
		}
	}while(!isCorrect && (countAvailableBytes(config) > 0));
	return (isCorrect) ? 0 : ret + 1;	// Most functions will only care if we got something correct eventually.
}

static char *addQuotations(const char *ptr){
	size_t len = strlen(ptr);
	char *ret = malloc((len + 3)*sizeof(char));
	if(ret != NULL){
		memcpy(ret + 1, ptr, len);
		ret[0] = ret[++len] = '\"';
		ret[++len] = '\0';
	}
	return ret;
}

static char *removeQuotations(const char *ptr){
	size_t len = strlen(ptr);
	char *ret = calloc((len - 1), sizeof(char));
	if(ret != NULL){
		memcpy(ret, ptr + 1, len - 2);
		//ret[len - 2] = '\0';
	}
	return ret;
}


// Public functions
bool at_checkReady(const AT_UART_Config *config){
	return (sendCommand_Burn(config, at_CHK, at_OK, 50, "checkReady") == 0);
}

void at_setEchoMode(const AT_UART_Config *config, bool setOnEcho){
	sendCommand_Burn(config, atw_ECHO(setOnEcho), at_OK, 50, "setEchoMode");
}

void at_powerOff(const AT_UART_Config *config){
	sendCommand_Burn(config, atw_POWERDOWN_NORMAL, atw_POWERDOWN_RSP, 3000, "powerOff");
}

int_fast16_t at_getCredit(const AT_UART_Config *config, const char *number){
	int_fast16_t ret = -1;
	float money = 0.0;
	char *dcs = atw_UNSTRUCTUREDDATA_DCS, *qnum = addQuotations(number),
		*ptr = malloc((sizeof(atw_UNSTRUCTUREDDATA) + strlen(qnum) + strlen(dcs) + 2)*sizeof(char)),
		*rsp;
	if(ptr != NULL){
		strcpy(ptr, atw_UNSTRUCTUREDDATA);
		strcat(ptr, qnum);
		strcat(ptr, ",");
		strcat(ptr, dcs);
		free(qnum);

		rsp = sendCommand_getResponse(config, ptr, 3000);
		free(ptr);

		if(rsp != NULL){
			// Find the monetary entry
			ptr = strchr(rsp, atw_UNSTRUCTUREDDATA_RSP_DELIM);
			if(ptr != NULL){
				ptr++;// Increment to the first digit
				money = atof(ptr);
				if(money != 0.0){
					ret = money*100;
				}
			}
			free(rsp);
		}
	}
	// Burn the OK in the buffer
	sendCommand_Burn(config, NULL, at_OK, 0, "getCredit");
	return ret;
}

static uint_fast16_t getPhonebookInfo(const AT_UART_Config *config, uint_fast32_t infoIdx){
	uint_fast16_t ret = 0, idx = 0;
	char *rsp = sendCommand_getResponse(config, atr_SELECTPHONEBOOKSTORAGE, 1000), *temp;
	sendCommand_Burn(config, NULL, at_OK, 0, "getPhonebookInfo");
	if(rsp != NULL && (strstr(rsp, c_SELECTPHONEBOOKSTORAGE) != NULL)){
		temp = strtok(rsp, at_RESPONSE_DELIMS);
		while(idx++ != infoIdx){
			temp = strtok(NULL, at_RESPONSE_DELIMS);
		}
		ret = atoi(temp);
	}
	free(rsp);
	return ret;
}

// Return the index of the contact in memory or -1 if not present
static int_fast16_t containsContact(const AT_UART_Config *config, const char *id){
	int_fast16_t ret = -1, idx;
	char *idq = addQuotations(id), *ptr, *temp = NULL;
	size_t idl = strlen(idq);

	ptr = malloc(sizeof(atw_FINDPHONEBOOKENTRY) + idl);
	if(ptr != NULL){
		strcpy(ptr, atw_FINDPHONEBOOKENTRY);
		strcat(ptr, idq);
		// Strange instruction; returns numbers containing the id, not exact matches
		temp = sendCommand_getResponse(config, ptr, 1000); // Give 3 second timeout max (reference manual)
		free(ptr);
		while(temp != NULL && strlen(temp) != (sizeof(at_OK) - 1) && strlen(temp) != (sizeof(at_ERROR) - 1)){	// Just compare sizes
			ptr = strtok(temp, at_RESPONSE_DELIMS);	// Tokenise response; should give +CPBF OR +CBPF
			if(ptr != NULL && ret == -1 &&	// Be cautious & skip parsing if located contact
					((strstr(ptr, c_FINDPHONEBOOKENTRY) != NULL) || (strstr(ptr, c_FINDPHONEBOOKENTRY_ALT) != NULL))){
				ptr = strtok(NULL, at_RESPONSE_DELIMS); // Gives the index of the number in memory
				idx = atoi(ptr);
				ptr = strtok(NULL, at_RESPONSE_DELIMS);	// Gives the number
				ptr = strtok(NULL, at_RESPONSE_DELIMS); // Gives the type of the number
				ptr = strtok(NULL, at_RESPONSE_DELIMS); // Gives the name tagged to the number
				if(strlen(ptr) == idl && !strncmp(ptr, idq, idl)){
					ret = idx;	// Contact already in memory
				}
			}
			free(temp);
			temp = sendCommand_getResponse(config, NULL, 0); // Get the next number or OK
		}
	}
	free(idq);
	free(temp);
	return ret;
}

bool at_addContact(const AT_UART_Config *config, const char *number, const char *id, bool nationalNumber){
	bool ret = false;
	if(containsContact(config, id) == -1){	// Only add if same name not present
		char *numberq = addQuotations(number), *idq = addQuotations(id),
						*natq = c_ISNATIONALNUMBER(nationalNumber), *ptr;
		size_t numberl = strlen(numberq), idl = strlen(idq), natl = strlen(natq);

		ptr = malloc(sizeof(atw_WRITEPHONEBOOKENTRY) + numberl + natl + idl + 3*sizeof(char));
		if(ptr != NULL){
			strcpy(ptr, atw_WRITEPHONEBOOKENTRY);
			strcat(ptr, ",");
			strcat(ptr, numberq);
			strcat(ptr, ",");
			strcat(ptr, natq);
			strcat(ptr, ",");
			strcat(ptr, idq);

			free(numberq);
			free(idq);
			free(natq);

			ret = (sendCommand_Burn(config, ptr, at_OK, 1000, "addContact") == 0);
			free(ptr);
		}
	}
	return ret;
}

bool at_removeContact(const AT_UART_Config *config, const char *id){
	bool ret = false;
	int_fast16_t idx;
	if((idx = containsContact(config, id)) != -1){
		char nBuffer[12], *ptr;
		memset(nBuffer, '\0', 12*sizeof(char));
		sprintf(nBuffer, "%d", idx);

		ptr = malloc(sizeof(atw_WRITEPHONEBOOKENTRY) + strlen(nBuffer));
		if(ptr != NULL){
			strcpy(ptr, atw_WRITEPHONEBOOKENTRY);
			strcat(ptr, nBuffer);

			ret = (sendCommand_Burn(config, ptr, at_OK, 1000, "removeContact") == 0);
			free(ptr);
		}
	}
	return ret;
}

void at_clearSIMContacts(const AT_UART_Config *config){
	char nBuffer[12], *ptr;
	uint_fast16_t idx, pos = sizeof(atw_WRITEPHONEBOOKENTRY) - 1,
			numToDelete = getPhonebookInfo(config, atr_SELECTPHONEBOOKSTORAGE_USED_IDX),
			total = getPhonebookInfo(config, atr_SELECTPHONEBOOKSTORAGE_TOTAL_IDX);

	memset(nBuffer, '\0', 12*sizeof(char));
	ptr = malloc(sizeof(atw_WRITEPHONEBOOKENTRY) + strlen(nBuffer));
	if(NULL != ptr){
		// Set the mode to PDU
		at_selectSMSFormat(config, false);
		strcpy(ptr, atw_WRITEPHONEBOOKENTRY);
		for(idx = 1; idx <= total && numToDelete > 0; idx++){
			sprintf(nBuffer, "%d", idx);
			ptr[pos] = '\0';
			strcat(ptr, nBuffer);
			if(sendCommand_Burn(config, ptr, at_OK, 250, "clearSIMContacts") == 0){
				numToDelete--;
			}
		}
		free(ptr);
	}
}

void at_selectSMSFormat(const AT_UART_Config *config, bool textMode){
	sendCommand_Burn(config, atw_SELECTSMSFMT(textMode), at_OK, 50, "selectSMSFormat");
}

bool at_sendSMS(const AT_UART_Config *config, const char *number, const char *message){
	bool ret = false, isText = false;
	size_t nLen = strlens(number), mLen = strlens(message);

	// TODO Breakup messages if exceed maximum length
	if(mLen > 640*sizeof(char)){
		return false;
	}

	// Check if in text mode
	isText = (sendCommand_Burn(config, att_SELECTSMSFMT_ISTEXT, att_SELECTSMSFMT_ISTEXT_RSP, 100, "sendSMS") == 0);
	sendCommand_Burn(config, NULL, at_OK, 0, "sendSMS"); // Get rid of the OK from the buffer
	if(isText){
		// Setup the text mode
		char *ptr = malloc(sizeof(atw_SENDSMS) + ((nLen + 3)*sizeof(char)));
		if(ptr != NULL){
			strcpy(ptr, atw_SENDSMS);

			// Surround our number with quotations
			char *n = addQuotations(number);
			strcat(ptr, n);
			free(n);

			// Send the command
			sendCommand_Burn(config, ptr, at_ENTERTEXT, 100, "sendSMS");
			free(ptr);

			// Send the actual message
			writeBytes(config, message, mLen, 100, true);

			// Send the escape character and expect an OK
			ret = sendCommand_Burn(config, at_ESCAPETEXT, at_OK, 100, "sendSMS"); // TODO Investigate why no response from modem
		}
	}
	return ret;
}

typedef struct{
	char **numbers;
	uint_fast16_t total;
} Phonebook;

static Phonebook *phonebook_init(const AT_UART_Config *config){
	Phonebook *ret = malloc(sizeof(Phonebook));
	if(ret != NULL){
		ret->total = getPhonebookInfo(config, atr_SELECTPHONEBOOKSTORAGE_USED_IDX);
		if(ret->total != 0){
			ret->numbers = calloc(ret->total, sizeof(char*));
			if(ret->numbers == NULL){
				free(ret);
				ret = NULL;
			}
		}
	}
	return ret;
}

static void phonebook_populate(const AT_UART_Config *config, Phonebook *pb){
	uint_fast32_t idx, numBytes = 0;

	char nBuffer[12], response[MAX_RESPONSE_SIZE], ptr[sizeof(atw_READPHONEBOOK) + 12*sizeof(char)],
			*temp;
	// Build the command using the static buffers
	memset(nBuffer, '\0', 12*sizeof(char));
	sprintf(nBuffer, "%d", getPhonebookInfo(config, atr_SELECTPHONEBOOKSTORAGE_TOTAL_IDX));
	strcpy(ptr, atw_READPHONEBOOK);
	strcat(ptr, nBuffer);

	writeBytes(config, ptr, strlen(ptr), 1000, true);
	for(idx = 0; idx < pb->total; idx++){
		numBytes = readBytes(config, response, MAX_RESPONSE_SIZE - 1, DEFAULT_RETRIES, RETRY_DELAY);
		response[numBytes] = '\0';
		if(numBytes != 0 && strstr(response, c_READPHONEBOOK) != NULL){	// Ensure it's a CPBR command
			temp = strtok(response, at_RESPONSE_DELIMS); // Tokenise response; gives +CPBR
			temp = strtok(NULL, at_RESPONSE_DELIMS);	// Gives the index of the number in memory

			temp = strtok(NULL, at_RESPONSE_DELIMS);	// Gives the number
			if(temp != NULL){
				pb->numbers[idx] = removeQuotations(temp);
			}
		}
		else{
			logError(config, "Expected a pb entry but issue encountered!");
		}
	}
	sendCommand_Burn(config, NULL, at_OK, 0, "populatePhoneBook");	// Burn the OK at the end; log anything else leftover
}

static void phonebook_destroy(Phonebook *pb){
	uint_fast32_t idx;
	if(pb != NULL){
		for(idx = 0; idx != pb->total; idx++){
			free(pb->numbers[idx]);
			pb->numbers[idx] = NULL;
		}
		if(pb->total != 0){
			free(pb->numbers);
			pb->numbers = NULL;
		}
		free(pb);
	}
}

bool at_sendBlanketSMS(const AT_UART_Config *config, const char *message){
	bool ret = false;
	uint_fast32_t idx;
	Phonebook *pb = phonebook_init(config);
	if(pb != NULL){
		phonebook_populate(config, pb);
		// Send the SMS to all SIM contacts
		for(idx = 0; idx != pb->total; idx++){
			if(pb->numbers[idx] != NULL){
				at_sendSMS(config, pb->numbers[idx], message);
				delayFunction(config, 5000);	// Small latency required
			}
		}
		phonebook_destroy(pb);
		pb = NULL;
	}
	return ret;
}

void at_deleteAllSMS(const AT_UART_Config *config){
	if((sendCommand_Burn(config, att_SELECTSMSFMT_ISTEXT,
			att_SELECTSMSFMT_ISTEXT_RSP, 100, "deleteAllSMS") == 0)){
		sendCommand_Burn(config, atw_DELETEALLSMS_TXT, at_OK, 5000, "deleteAllSMS");
	}
	else{
		sendCommand_Burn(config, atw_DELETEALLSMS_PDU, at_OK, 5000, "deleteAllSMS");
	}
}


IP_Connect_Status at_queryConnectionStatus(const AT_UART_Config *config){
	char response[MAX_RESPONSE_SIZE];
	size_t numBytes = 0, idx = 0,
			numElems = sizeof(ipConnectionStatuses)/sizeof(ipConnectionStatuses[0]);

	IP_Connect_Status ret = IP_ERROR;

	// Burn till an OK is reached; next command requires parsing; ASSUME IP related;
	sendCommand_Burn(config, ate_CHECKCONNECTIONSTATUS, at_OK, 100, "queryConnectionStatus");
	numBytes = readBytes(config, response, MAX_RESPONSE_SIZE, DEFAULT_RETRIES, RETRY_DELAY); // Read in the status

	if(numBytes >= sizeof(ate_CHECKCONNECTIONSTATUS_RSP)){ // Otherwise, known error
		for(; idx != numElems; idx++){
			if((strlen(ipConnectionStatuses[idx]) == numBytes) &&
					(strncmp(ipConnectionStatuses[idx], response, numBytes) == 0)){
				ret = (IP_Connect_Status)idx;
				break;
			}
		}
	}
	return ret;
}

bool at_checkGPRSServiceStatus(const AT_UART_Config *config){
	bool ret = (sendCommand_Burn(config, atr_ATTACHGPRS, atr_ATTACHGPRS_RSP, 100, "checkGPRSServiceStatus") == 0);
	sendCommand_Burn(config, NULL, at_OK, 0, "checkGPRSStatus");
	return ret;
}

void at_setupGPRSConnection(const AT_UART_Config *config, bool attach){
	sendCommand_Burn(config, atw_ATTACHGPRS(attach), at_OK, 250, "setupGPRSConnection");
}

// TODO
void enableTransparentMode(const AT_UART_Config *config){
	//sendCommand(config, "AT+CIPMODE=1", at_OK, 100);
}

bool at_setAPN(const AT_UART_Config *config, const char *apn, const char *usr, const char *pwd){
	bool ret = false;
	// Add quotations around all parameters and get lengths
	char *apnq = addQuotations(apn), *usrq = addQuotations(usr), *pwdq = addQuotations(pwd);
	size_t apnl = strlen(apnq), usrl = strlen(usrq), pwdl = strlen(pwdq);

	// Build up the command
	char *ptr = malloc(sizeof(atw_SETAPN) + apnl + usrl + pwdl + 2*sizeof(char));
	if(ptr != NULL){
		strcpy(ptr, atw_SETAPN);
		strcat(ptr, apnq);
		strcat(ptr, ",");
		strcat(ptr, usrq);
		strcat(ptr, ",");
		strcat(ptr, pwdq);

		free(apnq);
		free(usrq);
		free(pwdq);
		ret = (sendCommand_Burn(config, ptr, at_OK, 100, "setAPN") == 0);

		free(ptr);
	}
	return ret;
}

bool at_bringupWirelessConnection(const AT_UART_Config *config){
	return (sendCommand_Burn(config, ate_BRINGUPWIRELESS, at_OK, 200, "bringupWireless") == 0);
}

bool at_obtainIPAddress(const AT_UART_Config *config, char *ipAddress){
	bool ret = false;
	char response[MAX_RESPONSE_SIZE];
	size_t numBytes = 0;

	writeBytes(config, ate_GETLOCALIP, sizeof(ate_GETLOCALIP) - 1, 100, true);
	numBytes = readBytes(config, response, MAX_RESPONSE_SIZE, DEFAULT_RETRIES, RETRY_DELAY);

	if(strncmp(at_ERROR, response, numBytes) != 0 && numBytes <= 15){ // Check not error and less than max ip address length
		memcpy(ipAddress, response, numBytes);
		ret = true;
	}

	return ret;
}

bool at_establishTCPConnection(const AT_UART_Config *config, const char *ipAddress, const char *port){
	char *ipq = addQuotations(ipAddress), *portq = addQuotations(port);
	size_t ipl = strlen(ipq), portl = strlen(portq);

	char *ptr = malloc(sizeof(atw_STARTTCPCONNECTION) + ipl + portl + sizeof(char)); // For one comma
	if(ptr != NULL){
		strcpy(ptr, atw_STARTTCPCONNECTION);
		strcat(ptr, ipq);
		strcat(ptr, ",");
		strcat(ptr, portq);

		free(ipq);
		free(portq);

		sendCommand_Burn(config, ptr, at_OK, 3750, "establishTCPConnection");
		free(ptr);
	}
	// Only return connected if correct response after OK
	return (sendCommand_Burn(config, NULL, atw_STARTIPCONNECTION_RSP, 0, "establishTCPConnection") == 0);
}

bool at_sendTCPMessage(const AT_UART_Config *config, const char *message){
	bool ret = false;
	if(sendCommand_Burn(config, ate_SENDIPMESSAGE, ate_SENDIPMESSAGE_RSP, 50, "sendTCPMessage") == 0){
		writeBytes(config, message, strlens((const char*)message), 0, false);
		ret = (sendCommand_Burn(config, "\x0D\x0A\x1A\0", ate_SENDIPMESSAGE_RSP2, 1000, "sendTCPMessage") == 0);
	}
	return ret;
}

bool at_closeTCPConnection(const AT_UART_Config *config){
	return (sendCommand_Burn(config, ate_CLOSEIPCONNECTION, ate_CLOSEIPCONNECTION_RSP, 1000, "closeTCPConnection") == 0);
}

bool at_deactivatePDPContext(const AT_UART_Config *config){
	return 	(sendCommand_Burn(config, ate_DEACTIVATEPDPCONTEXT, ate_DEACTIVATEPDPCONTEXT_RSP, 1000, "deactivatePDPContext") == 0);
}

