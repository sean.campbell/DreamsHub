/*
 * modem_DREAMS.h
 *
 *  Created on: 23 Jan 2016
 *      Author: Sean
 */

#ifndef MODEM_DREAMS_H_
#define MODEM_DREAMS_H_

#include <stdbool.h>
#include <stdint.h>

void modem_powerOn(void);

void modem_powerOff(void);

void modem_emergencyReset(void);

int_fast16_t modem_getCredit(const char *number);

bool modem_addContact(const char *number, const char *id, bool isNationalNumber);

bool modem_removeContact(const char *id);

void modem_clearSIM();

void modem_sendSMS(const char *number, const char *message);

void modem_sendEmergencySMS(const char *message);

bool modem_openTCPConnection(const char *ipAddress, const char *port);

bool modem_sendTCPMessage(const char *message);

bool modem_getTCPMessage(char *buffer, uint_fast32_t maxLen);

bool modem_closeTCPConnection(const bool disconnectNetwork);

#endif /* MODEM_DREAMS_H_ */
