/*
 * modem_DREAMS.c
 *
 *  Created on: 23 Jan 2016
 *      Author: Sean
 */
#include "driverlib.h"
#include "modem_DREAMS.h"
#include "modem_ATC.h"
#include "clocking_DREAMS.h"

#include "circularBuffer.h"

#include <stdio.h>

// Precaution
#ifndef NULL
#define NULL ((void*)0)
#endif

// Changeable aspects
#define APN_NAME 			"pp.vodafone.co.uk"
#define APN_USERNAME		"web"
#define APN_PASSWORD		"web"

#define DREAMS_V2 // MODEM pins realocated ov Rev 2 board

// Modem port and pin connections

#ifdef DREAMS_V1
#define MODEM_RST_PORT 		GPIO_PORT_P2
#define MODEM_RST_PIN 		GPIO_PIN4

#define MODEM_STA_PORT 		GPIO_PORT_P5
#define MODEM_STA_PIN 		GPIO_PIN6

#define MODEM_PWK_PORT 		GPIO_PORT_P6
#define MODEM_PWK_PIN 		GPIO_PIN6
#endif

#ifdef DREAMS_V2
#define MODEM_RST_PORT 		GPIO_PORT_P2
#define MODEM_RST_PIN 		GPIO_PIN7

#define MODEM_STA_PORT 		GPIO_PORT_P2
#define MODEM_STA_PIN 		GPIO_PIN6

#define MODEM_PWK_PORT 		GPIO_PORT_P2
#define MODEM_PWK_PIN 		GPIO_PIN4
#endif

#define MODEM_RX_TX_PORT 	GPIO_PORT_P3
#define MODEM_TX_PIN 		GPIO_PIN2
#define MODEM_RX_PIN 		GPIO_PIN3
#define MODEM_UART 			EUSCI_A2_BASE
#define MODEM_UART_INT 		INT_EUSCIA2

// DEBUGGING FOR BACKCHANNEL ECHO
#undef DEBUG
#define DEBUG				(0)
#ifdef DEBUG
#define BACKCHANNEL_RX_TX_PORT	GPIO_PORT_P1
#define BACKCHANNEL_TX_PIN 		GPIO_PIN2
#define BACKCHANNEL_RX_PIN 		GPIO_PIN3
#define BACKCHANNEL_UART 		EUSCI_A0_BASE
#endif


// Modem delay times in ms
#define MODEM_PWK_DOWN_ON 1000
#define MODEM_PWK_UP_ON 2000
#define MODEM_STALL 15000

#define MODEM_PWK_DOWN_OFF 1000
#define MODEM_PWK_UP_OFF 1500

#define MODEM_RST_UP 1
#define MODEM_RST_DOWN 2200

static circularBuffer_t rxBuffer, rxUBuffer;

// Configure using:
// http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSP430BaudRateConverter/index.html
static const eUSCI_UART_Config modemUARTConfig = { // 9600 Baud Rate
        EUSCI_A_UART_CLOCKSOURCE_SMCLK,          // SMCLK Clock Source
        78,                                      // BRDIV = 78
        2,                                       // UCxBRF = 2
        0,                                       // UCxBRS = 0
        EUSCI_A_UART_NO_PARITY,                  // No Parity
        EUSCI_A_UART_LSB_FIRST,                  // LSB First
        EUSCI_A_UART_ONE_STOP_BIT,               // One stop bit
        EUSCI_A_UART_MODE,                       // UART mode
        EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION  // Oversampling
};

#ifdef DEBUG
const eUSCI_UART_Config PCUartConfig = {
		EUSCI_A_UART_CLOCKSOURCE_SMCLK,          // SMCLK Clock Source
		6,                                      // BRDIV = 26
		8,                                       // UCxBRF = 0
		0,                                       // UCxBRS = 0
		EUSCI_A_UART_NO_PARITY,                  // No Parity
		EUSCI_A_UART_LSB_FIRST,                  // MSB First
		EUSCI_A_UART_ONE_STOP_BIT,               // One stop bit
		EUSCI_A_UART_MODE,                       // UART mode
		EUSCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION  // Low Frequency Mode
		};
#endif

// Functions to isolate AT commands from the UART functionality
static inline void writeByteFunction(char data){
	// Wait for buffer to be ready
	while(!(UCA2IFG & UCTXIFG));
	MAP_UART_transmitData(MODEM_UART, data);
	MAP_UART_transmitData(BACKCHANNEL_UART, data);
	while(UCA2STATW & UCBUSY);
}

static inline bool readByteFunction(char *data){
	return circularBuffer_get(&rxBuffer, data);
}

static inline uint_fast32_t numBytesFunction(void){
	return circularBuffer_size(&rxBuffer);
}

static inline void writeErrorByteFunction(const char *data, uint_fast32_t len){
	uint_fast32_t idx;
	for(idx = 0; idx != len; idx++){
		circularBuffer_put(&rxUBuffer, data[idx]);
	}
	circularBuffer_put(&rxUBuffer, 0x0D);
	circularBuffer_put(&rxUBuffer, 0x0A);
}


static const AT_UART_Config atcConfig = {
		writeByteFunction,

		readByteFunction,
		numBytesFunction,

		timer32_delay,

		writeErrorByteFunction,

		NULL
};

static void setupUART(void){
	// Allocate UART buffers
	circularBuffer_init(&rxBuffer);
	circularBuffer_init(&rxUBuffer);

	// Setup UART to Modem
	MAP_GPIO_setAsPeripheralModuleFunctionOutputPin(MODEM_RX_TX_PORT,
			MODEM_TX_PIN | MODEM_RX_PIN, GPIO_PRIMARY_MODULE_FUNCTION);
	MAP_UART_initModule(MODEM_UART, &modemUARTConfig);
	MAP_UART_enableModule(MODEM_UART);

	// Setup UART to PC if in debug mode
#ifdef DEBUG
	MAP_GPIO_setAsPeripheralModuleFunctionOutputPin(BACKCHANNEL_RX_TX_PORT,
			BACKCHANNEL_TX_PIN | BACKCHANNEL_RX_PIN, GPIO_PRIMARY_MODULE_FUNCTION);
	MAP_UART_initModule(BACKCHANNEL_UART, &PCUartConfig);
	MAP_UART_enableModule(BACKCHANNEL_UART);
#endif

	// Enable interrupts
	MAP_UART_enableInterrupt(MODEM_UART, EUSCI_A_UART_RECEIVE_INTERRUPT);
	MAP_Interrupt_enableInterrupt(MODEM_UART_INT);
}

#define MAX_POWERON_RETRIES		4
void modem_powerOn(void){
	uint_fast32_t retries = 0;
	// Set RST as output
	MAP_GPIO_setAsOutputPin(MODEM_RST_PORT, MODEM_RST_PIN);
	MAP_GPIO_setOutputLowOnPin(MODEM_RST_PORT, MODEM_RST_PIN);

	// Set STA as input
	MAP_GPIO_setAsInputPin(MODEM_STA_PORT, MODEM_STA_PIN);

	// Check if modem is not outputting anything on status; if nothing following power off and auto powers on
	if(MAP_GPIO_getInputPinValue(MODEM_STA_PORT, MODEM_STA_PIN) == GPIO_INPUT_PIN_LOW){
		// Set PWK as output
		MAP_GPIO_setAsOutputPin(MODEM_PWK_PORT, MODEM_PWK_PIN);
		MAP_GPIO_setOutputLowOnPin(MODEM_PWK_PORT, MODEM_PWK_PIN);
		timer32_delay(MODEM_PWK_DOWN_ON);

		MAP_GPIO_setOutputHighOnPin(MODEM_PWK_PORT, MODEM_PWK_PIN);
		timer32_delay(MODEM_PWK_UP_ON);

		while(MAP_GPIO_getInputPinValue(MODEM_STA_PORT, MODEM_STA_PIN) == GPIO_INPUT_PIN_LOW && retries != MAX_POWERON_RETRIES){
			timer32_delay(MODEM_STALL);
			retries++;
		}
	}

	static bool flag = false;
	// Setup UART peripherals only once
	if(!flag){
		setupUART();
		flag = true;
	}
	bool breakFlag = false;
	while(!breakFlag && retries != MAX_POWERON_RETRIES){
		breakFlag = at_checkReady(&atcConfig);
		retries++;
	}
	at_setEchoMode(&atcConfig, false); // Assume always successful; burn through buffer now anyway.
}

int_fast16_t modem_getCredit(const char *number){
	at_selectSMSFormat(&atcConfig, false);
	return at_getCredit(&atcConfig, number);
}

bool modem_addContact(const char *number, const char *id, bool isNationalNumber){
	return at_addContact(&atcConfig, number, id, isNationalNumber);
}

bool modem_removeContact(const char *id){
	return at_removeContact(&atcConfig, id);
}

void modem_clearSIM(){
	// Clear contacts + delete all SMS messages
	at_clearSIMContacts(&atcConfig);
	at_deleteAllSMS(&atcConfig);
}

void modem_sendSMS(const char *number, const char *message){
	at_selectSMSFormat(&atcConfig, true);
	at_sendSMS(&atcConfig, number, message);
}

void modem_sendEmergencySMS(const char *message){
	at_selectSMSFormat(&atcConfig, true);
	at_sendBlanketSMS(&atcConfig, message);
}

#define CHECK_SERVICE_LIMIT 3		// Permitted attempts before reset of modem
#define CHECK_SERVICE_FAIL_LIMIT 2	// Permitted resets

static bool connectGPRSService(){
	uint8_t idx, breakLim = 0;
	bool ncFlag = true;
	do{
		for(idx = 0; idx != CHECK_SERVICE_LIMIT; idx++){
			if(!at_checkGPRSServiceStatus(&atcConfig)){
				at_setupGPRSConnection(&atcConfig, true);
			}
			else{
				ncFlag = false;
				break;
			}
			timer32_delay(1000);
		}
		if(ncFlag){
			modem_powerOff();
			modem_powerOn();
			//modem_emergencyReset();
			breakLim++;
		}
	}while(ncFlag && (breakLim < CHECK_SERVICE_FAIL_LIMIT));

	return !ncFlag;
}

#define TCP_CONNECTING_MAX_RETRIES		3

// "AT+CSTT=\"pp.vodafone.co.uk\",\"web\",\"web\""
bool modem_openTCPConnection(const char *hostName, const char *port){
	bool ret = false;
	uint_fast16_t retries = TCP_CONNECTING_MAX_RETRIES;

	if(connectGPRSService()){
		IP_Connect_Status status = at_queryConnectionStatus(&atcConfig);
		if(status == PDP_DEACT){
			at_deactivatePDPContext(&atcConfig);
			status = at_queryConnectionStatus(&atcConfig);
		}

		if(status == IP_INITIAL){
			at_setAPN(&atcConfig, APN_NAME, APN_USERNAME, APN_PASSWORD); // Should set state to IP_START
			status = at_queryConnectionStatus(&atcConfig);
		}

		if(status == IP_START){ // Should set state to IP_GPRSACT
			at_bringupWirelessConnection(&atcConfig);
			status = at_queryConnectionStatus(&atcConfig);
		}

		if(status == IP_GPRSACT){
			char ipAddressBuffer[16];
			at_obtainIPAddress(&atcConfig, ipAddressBuffer);
			status = at_queryConnectionStatus(&atcConfig);
		}

		if(status == IP_STATUS || status == TCP_CLOSED){
			at_establishTCPConnection(&atcConfig, hostName, port);
			status = at_queryConnectionStatus(&atcConfig);
		}

		while(status == TCP_CONNECTING && (retries-- > 0)){	// Untested
			timer32_delay(1000);
			status = at_queryConnectionStatus(&atcConfig);
		}

		if(status == CONNECT_OK){
			ret = true;
		}
	}

	return ret;
}

bool modem_sendTCPMessage(const char *message){
	return at_sendTCPMessage(&atcConfig, message);
}

// Put unsolicited responses into the unexpected buffer
static void readAllIntoUnexpectedBuffer(){
	char readChar = '\0';
	// Small latency required?
	timer32_delay(1000);
	while(!circularBuffer_isEmpty(&rxBuffer)){
		circularBuffer_get(&rxBuffer, &readChar);
		circularBuffer_put(&rxUBuffer, readChar);
	}
}

// Read a message ending with <CR><LF>
static uint_fast32_t readMessage(char *buffer, uint_fast32_t maxLen){
	bool sawCR = false, gotChar = false;
	char readByte;
	uint_fast32_t idx = 0;

	while(idx < maxLen && !circularBuffer_isEmpty(&rxUBuffer)){
		gotChar = circularBuffer_get(&rxUBuffer, &readByte);
		if(gotChar && readByte != 0x0D && readByte != 0x0A){
			buffer[idx++] = readByte;
		}
		if(idx > 1){
			if(readByte == 0x0D){
				sawCR = true;
			}
			else if(sawCR && readByte == 0x0A){
				break;
			}
		}
	}
	return idx;
}

// Only return a message that could be JSON in origin
bool modem_getTCPMessage(char *buffer, uint_fast32_t maxLen){
	uint_fast32_t numBytes = 0;
	bool breakFlag = false;
	readAllIntoUnexpectedBuffer();

	while(!breakFlag && !circularBuffer_isEmpty(&rxUBuffer)){
		numBytes = readMessage(buffer, maxLen - 1);
		if(numBytes > 0 && buffer[0] == '{'){
			breakFlag = true;
			buffer[numBytes] = '\0';	// Null terminate
		}
	}
	return breakFlag;
}

bool modem_closeTCPConnection(bool disconnectNetwork){
	bool ret = false;
	uint_fast16_t retries = TCP_CONNECTING_MAX_RETRIES;

	IP_Connect_Status status = at_queryConnectionStatus(&atcConfig);

	if(status == IP_STATUS || status == CONNECT_OK){
		at_closeTCPConnection(&atcConfig);
		status = at_queryConnectionStatus(&atcConfig);

		while(status == TCP_CONNECTING && (retries-- > 0)){	// Untested
			timer32_delay(1000);
			status = at_queryConnectionStatus(&atcConfig);
		}
	}

	if(disconnectNetwork && status == TCP_CLOSED){
		at_setupGPRSConnection(&atcConfig, false);
		status = at_queryConnectionStatus(&atcConfig);
	}

	if(disconnectNetwork && status == PDP_DEACT){
		at_deactivatePDPContext(&atcConfig);
		status = at_queryConnectionStatus(&atcConfig);
	}

	if((status == TCP_CLOSED && !disconnectNetwork)
			|| (status == IP_INITIAL && disconnectNetwork)){ // Disconnect connection or network
		ret = true;
	}
	return ret;
}

// The break-out board we're using always powers up the SIM800 automatically;
// power off receives the "normal power down" response but then auto-powers back up...
void modem_powerOff(void){
	at_powerOff(&atcConfig);
	MAP_GPIO_setOutputLowOnPin(MODEM_PWK_PORT, MODEM_PWK_PIN);
	// At this point, "normal power down" read into receive buffer and auto powered back on
	// if one uses PWK.  Try AT Command?
}

void modem_emergencyReset(void){
	MAP_GPIO_setOutputHighOnPin(MODEM_RST_PORT, MODEM_RST_PIN);
	timer32_delay(MODEM_RST_UP);
	MAP_GPIO_setOutputLowOnPin(MODEM_RST_PORT, MODEM_RST_PIN);
	timer32_delay(MODEM_RST_DOWN);

	while(MAP_GPIO_getInputPinValue(MODEM_STA_PORT, MODEM_STA_PIN) == GPIO_INPUT_PIN_LOW){
		timer32_delay(MODEM_STALL);
	}
}

void euscia2_isr(void){
	// Read the byte
	uint_fast8_t data = UART_receiveData(MODEM_UART);
#ifdef DEBUG
	MAP_UART_transmitData(BACKCHANNEL_UART, data);
#endif
	circularBuffer_put(&rxBuffer, data);

	// Clear interupts
    uint32_t status = MAP_UART_getEnabledInterruptStatus(MODEM_UART);
    MAP_UART_clearInterruptFlag(MODEM_UART, status);
}

