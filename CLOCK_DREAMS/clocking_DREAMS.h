/** @file clocking_DREAMS.h
 *  @brief Interface for the Clocking API.
 *
 *  This file declares function prototypes for the
 *  clocking API in addition to declaring the clock
 *  rates to be utilised.
 *
 *  @author Sean (SeanCampbell)
 *
 *  @bug No known bugs.
 */
#ifndef CLOCKING_DREAMS_H_
#define CLOCKING_DREAMS_H_

#include <stdint.h>
#include <stdbool.h>

/*!
 *  @brief MCLK set to 24 MHz using MODCLK
 */
#define MCLK_RATE 24000000
/*!
 *  @brief SMMCLK set to 12 MHz using MODCLK with
 *  a prescale of 2
 */
#define SMCLK_RATE 12000000

/*! @brief Clock System setup
 *
 *  Setup the necessary MSP432 clock resources and clock signals
 */
void setupClocks(void);

/*! @brief Timer32 Module setup
 *
 *  Setup the ARM licensed Timer32 module
 */
void timer32_setup(void);

/*! @brief System stall using the Timer32 module
 *
 *  Delay the system by an arbitrary number of milliseconds,
 *  forcing the system into a low-power but interrupt-aware
 *  state
 *
 * 	@param msDelay The number of milliseconds to reside in
 * 	low-power mode
 */
void timer32_delay(uint32_t msDelay);

#endif /* CLOCKING_DREAMS_H_ */
