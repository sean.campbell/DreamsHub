/** @file clocking_DREAMS.c
 *  @brief Function definitions for the Clocking API.
 *
 *  @author Sean (SeanCampbell)
 *
 *  @bug No known bugs.
 */

#include "clocking_DREAMS.h"
#include "driverlib.h"

/*
 *  Static flag used to check if it's time to wake-up
 *  from low-power mode
 */
static volatile bool waitT32 = true;

void setupClocks(void){
	// Operate MCLK at 24MHz
	CS_initClockSignal(CS_MCLK, CS_MODOSC_SELECT, CS_CLOCK_DIVIDER_1);
	// Operate SMCLK at 12MHz for UART (lowest error rate for reception + transmission)
	CS_initClockSignal(CS_SMCLK, CS_MODOSC_SELECT, CS_CLOCK_DIVIDER_2);
	// Initialise timers
	timer32_setup();
}

void timer32_setup(void){
	MAP_Timer32_initModule((uint32_t)TIMER32_0_BASE, TIMER32_PRESCALER_16, TIMER32_32BIT,
			TIMER32_PERIODIC_MODE);
	MAP_Interrupt_enableInterrupt(INT_T32_INT1);
	MAP_Timer32_enableInterrupt((uint32_t)TIMER32_0_BASE);
}

void timer32_delay(uint32_t msDelay){
	uint64_t sDelay = 1500*msDelay;
	if(sDelay > UINT32_MAX)	// Prevent overflow; best effort timer
		sDelay = UINT32_MAX;

	MAP_Timer32_setCount((uint32_t)TIMER32_0_BASE, (uint32_t)sDelay);
	MAP_Timer32_clearInterruptFlag((uint32_t)TIMER32_0_BASE);
	waitT32 = true;

	MAP_Timer32_startTimer((uint32_t)TIMER32_0_BASE, true);
	while(waitT32){	// Ignore other interrupts on the processor end
		MAP_PCM_gotoLPM0InterruptSafe();
	}
}

void timer32_isr(void){
    MAP_Timer32_clearInterruptFlag((uint32_t)TIMER32_0_BASE);
    waitT32 = false;
}
