# README #

Repository for storing work done on implementing the DREAMS Hub subsystem. As well as a project containing the software for the DREAMS Hub it will also contain projects showcasing and testing different functionality required for the project. 

This subsystem is being implemented on the MSP432 microcontroller and the majority will be written in C.