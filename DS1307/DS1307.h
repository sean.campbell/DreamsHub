/*
 * DS1307.h
 *
 *  Created on: 20 Feb 2016
 *      Author: David
 */

#include "driverlib.h"

#include <stdio.h>

#ifndef DS1307_H_
#define DS1307_H_

#define SLAVE_ADDR 0x68
#define TIMEOUT 500

#define I2C_MODULE EUSCI_B1_BASE
#define I2C_PORT GPIO_PORT_P6
#define I2C_SDA_PIN GPIO_PIN5
#define I2C_SCL_PIN GPIO_PIN4

typedef struct  timeS
{
  uint8_t sec;
  uint8_t min;
  uint8_t hr;

  uint8_t wDay;
  uint8_t day;
  uint8_t mth;
  uint16_t yr;

} timeS;

void initRTC();
bool getTime(timeS* t);
bool setTime(timeS* t);
bool rtcExist();

char* getStringTime();
char* getStringDate();
char* getStringDateTime();

#endif /* DS1307_H_ */

