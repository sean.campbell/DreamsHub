/*
 * DS1307.c
 *
 *  Created on: 20 Feb 2016
 *      Author: David
 */

#include "DS1307.h"
#include "clocking_DREAMS.h"

// Is DS1307 connected
bool exists = false;

// Configuration for I2C bus
const eUSCI_I2C_MasterConfig i2cConfig = {
EUSCI_B_I2C_CLOCKSOURCE_SMCLK, // SMCLK Clock Source
	SMCLK_RATE, // SMCLK = 12MHz
	EUSCI_B_I2C_SET_DATA_RATE_100KBPS, // DS1307 only works in slow mode
	0, // No byte counter threshold
	EUSCI_B_I2C_NO_AUTO_STOP // No Autostop
	};

// From Arduino library
// Convert Decimal to Binary Coded Decimal (BCD)
uint8_t dec2bcd(uint8_t num) {
	return ((num / 10 * 16) + (num % 10));
}

// From Arduino library
// Convert Binary Coded Decimal (BCD) to Decimal
uint8_t bcd2dec(uint8_t num) {
	return ((num / 16 * 10) + (num % 16));
}


void initRTC() {

    MAP_GPIO_setAsPeripheralModuleFunctionInputPin(I2C_PORT,
    	I2C_SDA_PIN | I2C_SCL_PIN, GPIO_PRIMARY_MODULE_FUNCTION);
	/* Initializing I2C Master to SMCLK at 400kbs with no autostop */
	MAP_I2C_initMaster(I2C_MODULE, &i2cConfig);
	/* Specify slave address */
	MAP_I2C_setSlaveAddress(I2C_MODULE, SLAVE_ADDR);

	/* Enable I2C Module to start operations */
	MAP_I2C_enableModule(I2C_MODULE);

    /* Enable and clear the interrupt flag */
    MAP_I2C_clearInterruptFlag(I2C_MODULE,
            EUSCI_B_I2C_TRANSMIT_INTERRUPT0 + EUSCI_B_I2C_RECEIVE_INTERRUPT0);

    MAP_I2C_enableInterrupt(I2C_MODULE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0);

}

uint8_t readI2C(){
	// Wait until buffer is full
	int i = 0;
	while(!(MAP_I2C_getEnabledInterruptStatus(I2C_MODULE) & EUSCI_B_I2C_RECEIVE_INTERRUPT0)){
    	if(i > TIMEOUT) return 0;
    	i++;
	}
	return MAP_I2C_masterReceiveMultiByteNext(I2C_MODULE);
}

bool writeI2C(uint8_t dat){
	bool ret = MAP_I2C_masterSendMultiByteNextWithTimeout(I2C_MODULE, dat, TIMEOUT);
	MAP_I2C_clearInterruptFlag(I2C_MODULE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0);
    return ret;
}

// Aquire data from the RTC chip in BCD format
bool readTime(timeS* tm) {
	uint8_t sec;

	int i = 0;
	/* Set Master in receive mode */
	MAP_I2C_setMode(I2C_MODULE, EUSCI_B_I2C_TRANSMIT_MODE);

	bool res = false;

	res = MAP_I2C_masterSendMultiByteStartWithTimeout(I2C_MODULE, (uint8_t)0x00, TIMEOUT);

	// wait for interrupt
    while(!MAP_I2C_getInterruptStatus(I2C_MODULE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0)){
			if(i > TIMEOUT) {
				res = false;
				break;
			}
			i++;
		};

    MAP_I2C_clearInterruptFlag(I2C_MODULE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0);

	if (res) {
		exists = true;

		// disable transmit interrupt
		MAP_I2C_disableInterrupt(I2C_MODULE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0);

		// change mode
		MAP_I2C_setMode(I2C_MODULE, EUSCI_B_I2C_RECEIVE_MODE);
		// start recieve
		MAP_I2C_masterReceiveStart(I2C_MODULE);
		// enable rcv interrupt
		MAP_I2C_enableInterrupt(I2C_MODULE, EUSCI_B_I2C_RECEIVE_INTERRUPT0);

		sec = readI2C();
		tm->sec = bcd2dec(sec & 0x7f);
		tm->min = bcd2dec(readI2C());
		tm->hr = bcd2dec(readI2C() & 0x3f); // assumes 24hr
		tm->wDay = bcd2dec(readI2C());
		tm->day = bcd2dec(readI2C());
		tm->mth = bcd2dec(readI2C());
		// Stop before last byte read
		MAP_I2C_masterReceiveMultiByteStop(I2C_MODULE);
	    tm->yr = (bcd2dec(readI2C()) + 2000);

		MAP_I2C_disableInterrupt(I2C_MODULE, EUSCI_B_I2C_RECEIVE_INTERRUPT0);

		if (sec & 0x80) res = false; // clock is halted
	}

	return res;
}


bool writeTime(timeS* tm) {

	/* Set Master in receive mode */
	MAP_I2C_setMode(I2C_MODULE, EUSCI_B_I2C_TRANSMIT_MODE);

	bool res = false;

	res = MAP_I2C_masterSendMultiByteStartWithTimeout(I2C_MODULE, (uint8_t)0x00, TIMEOUT);
    MAP_I2C_clearInterruptFlag(I2C_MODULE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0);

	// if start was successful, send data
	if (res) {
		if(!writeI2C(dec2bcd(tm->sec))) return false;
		if(!writeI2C(dec2bcd(tm->min))) return false;
		if(!writeI2C(dec2bcd(tm->hr))) return false;
		if(!writeI2C(dec2bcd(tm->wDay))) return false;
		if(!writeI2C(dec2bcd(tm->day))) return false;
		if(!writeI2C((uint8_t)dec2bcd(tm->mth))) return false;

		//Write final byte
		MAP_I2C_masterSendMultiByteFinishWithTimeout(I2C_MODULE, dec2bcd(tm->yr - 2000), TIMEOUT);
	    while(!MAP_I2C_getInterruptStatus(I2C_MODULE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0));
	    MAP_I2C_clearInterruptFlag(I2C_MODULE, EUSCI_B_I2C_TRANSMIT_INTERRUPT0);
	}

	exists = res;
	return res;
}

char* getStringDate(){
	//ddMMyyyy
	static char res[9];

	timeS time;
	bool rtcExist = getTime(&time);

	// Check if chip is present and running
	if(rtcExist){
	snprintf(res, 9, "%02d%02d%04d", time.day, time.mth, time.yr);
	}
	else{
		snprintf(res, 9, "00000000");
	}
	return res;
}

char* getStringTime(){
	//hhmmss
	static char res[7];

	timeS time;
	bool rtcExist = getTime(&time);

	// Check if chip is present and running
	if(rtcExist) {
		snprintf(res, 7, "%02d%02d%02d", time.hr, time.min, time.sec);
	}
	else{
		snprintf(res, 7, "000000");
	}

	return res;
}

char* getStringDateTime(){
	//hhmmss
	static char res[15];

	timeS time;
	bool rtcExist = getTime(&time);

	// Check if chip is present and running
	if(rtcExist) {
		snprintf(res, 15, "%02d%02d%02d%02d%02d%04d", time.hr, time.min, time.sec, time.day, time.mth, time.yr);
	}
	else{
		snprintf(res, 15, "00000000000000");
	}

	return res;
}

bool getTime(timeS* t)   // Aquire data from buffer and convert to time_t
{
	return readTime(t);
}

bool setTime(timeS* t) {

	bool res;

	t->sec |= 0x80;  // stop the clock
	res = writeTime(t);
	t->sec &= 0x7f;  // start the clock
	res = writeTime(t);

	return res;
}
