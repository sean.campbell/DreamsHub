//*****************************************************************************
//
// MSP432 main.c template - P1.0 port toggle
//
//****************************************************************************

#include "msp.h"

void main(void)
{
    volatile uint32_t i;

    WDT_A->rCTL.r = WDTPW | WDTHOLD;    // Stop watchdog timer

    // The following code toggles P1.0 port
    DIO->rPADIR.b.bP1DIR |= BIT0;       // Configure P1.0 as output

    while(1)
    {
        DIO->rPAOUT.b.bP1OUT ^= BIT0;   // Toggle P1.0
        for(i=10000; i>0; i--);         // Delay
    }
}
