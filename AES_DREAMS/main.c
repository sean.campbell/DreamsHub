//*****************************************************************************
//
// MSP432 main.c template - Empty main
//
//****************************************************************************
#include "aes_Dreams.h"
#include <stdlib.h> // Dynamic memory alloc & NULL
#include <string.h> // Memset & Memcpy
#include <msp.h>

void testBasic(){
	const uint8_t data[16] =
		{ 0x01, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
		  0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff};

	uint8_t encryptedData[16], decryptedData[16];
	uint16_t idx;

	encryptData(&data, &encryptedData);
	decryptData((const uint8_t (*)[16])&encryptedData, &decryptedData);
	for(idx = 0; idx < 16; idx++){
		if(data[idx] != decryptedData[idx])
			while(1);	// For debugging
	}
}

void testComplex(){
	const uint8_t data[34] =
		{ 0x01, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
		  0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		  0x88, 0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff,
		  0x01, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
		  0xef, 0xed};

	const uint8_t iv[16] =
		{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
		  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
		};

	uint16_t idx, sz = 34, *pSz = &sz;

	const uint8_t *enc = encrypt(data, pSz, iv);
	const uint8_t *dec = decrypt(enc, sz, iv);
	for(idx = 0; idx < 34; idx++){
		if(data[idx] != dec[idx])
			while(1);	// For debugging
	}
	free((void*)enc);
	free((void*)dec);
}

void main(void){
	//uint16_t sz = 18, *pSz = &sz, idx;
    WDTCTL = WDTPW | WDTHOLD;           // Stop watchdog timer
	
    testBasic();

    testComplex();
}

