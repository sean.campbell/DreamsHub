/*
 * aes_Dreams.h
 *
 *
 *  Created on: 16 Dec 2015
 *      Author: Sean
 */

#ifndef AES_DREAMS_AES_DREAMS_H_
#define AES_DREAMS_AES_DREAMS_H_

#include <stdint.h> 	// Standard uint8_t type
#include <stdbool.h>

// Key Info
#define AES_KL_MODE AESKL__128BIT
#define AES_KL (128 / 8)

/**
 * Set the AES key with pre-defined length used for the next operation
 */
void setAESKey(const uint8_t (*key)[AES_KL]);

/**
 * Explicit set-up of accelerator with the set symmetric key
 */
void setupAESAccelerator(void);

/*
 * Reset all accelerator registers
 */
void resetAESAccelerator(void);

/**
 * Very low-level encrypt function
 * Start the operation to encrypt supplied data using 128-bit
 * AES encryption but do not poll till finished
 */
void startEncryptData(const uint8_t (*data)[16]);

/**
 * Very low-level decrypt function
 * Start the operation to decrypt supplied data using 128-bit
 * AES encryption but do not poll till finished
 */
void startDecryptData(const uint8_t (*encryptedData)[16]);

/**
 * Attempt to read out any data stored in accelerator
 * If the accelerator is not busy, supplied buffer will contain
 * relevant data and true returned
 * Otherwise, supplied buffer will be unaltered and false
 * returned
 */
bool readData(uint8_t (*data)[16]);

/**
 * Check if the accelerator is encrypting, decrypting or if
 * a key generation is in progress
 * Return true if busy, otherwise false
 */
bool isAESAcceleratorBusy(void);

/**
 * Low-level encrypt function
 * Encrypts data supplied using 128-bit AES encryption and stores
 * the result in the supplied buffer
 */
void encryptData(const uint8_t (*data)[16], uint8_t (*encryptedData)[16]);

/**
 * Low-level decrypt function
 * Decrypts data supplied using 128-bit AES encryption and stores
 * the result in the supplied buffer
 */
void decryptData(const uint8_t (*encryptedData)[16], uint8_t (*data)[16]);

/**
 * High-level encrypt function
 * Encrypts data using 128-bit AES encryption on 16 byte blocks and
 * stores the result in a dynamically allocated array; value of length
 * altered to reflect the actual dynamic length
 * Optionally uses software defined CBC on top of hardware ECB
 */
uint8_t *encrypt(const uint8_t *data, uint16_t *len, const uint8_t *iv);

/**
 * High-level decrypt function
 * Decrypts data using 128-bit AES encryption on 16 byte blocks and
 * stores the result in a dynamically allocated array; assumes length
 * will be a multiple of 16
 * Optionally uses software defined CBC on top of hardware ECB
 */
uint8_t *decrypt(const uint8_t *encryptedData, const uint16_t len, const uint8_t *iv);

//void registerAESInterrupt(void);
//void unregisterAESInterrupt(void);

#endif /* AES_DREAMS_AES_DREAMS_H_ */
