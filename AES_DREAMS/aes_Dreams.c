/*
 * aes_Dreams.c
 *
 *  Created on: 16 Dec 2015
 *      Author: Sean
 */

#include "aes_DREAMS.h"

#include <stdlib.h> // Dynamic memory alloc & NULL
#include <string.h> // Memset & Memcpy
#include <msp.h>	// AES register info

//key = 3E 07 31 59 03 CB 4B 45 14 6E 66 94 D8 04 B6 ED
static uint8_t cipherKey[AES_KL] =
	{ 0x3E, 0x07, 0x31, 0x59, 0x03, 0xCB, 0x4B, 0x45,
	  0x14, 0x6E, 0x66, 0x94, 0xD8, 0x04, 0xB6, 0xED};

static bool keyChanged = false;

void setAESKey(const uint8_t (*key)[AES_KL]){
	memcpy(cipherKey, key, sizeof(cipherKey));
	keyChanged = true;
}

void setupAESAccelerator(void){
	uint16_t sCipherKey, idx;
	// Set to encrypt mode
	AESACTL0 &= ~AESOP_3;

	// Set key length
	AESACTL0 = AESACTL0 & (~(AESKL_1 + AESKL_2))  | AES_KL_MODE;

	// Set to ECB mode
	AESACTL0 = AESACTL0 & (~AESCM_3) | AESCM__ECB;

	// Load symmetric key into register
	for(idx = 0; idx < AES_KL; idx = idx + 2){
		// Concatenate into single 16-bit block and load to register
		sCipherKey =(uint16_t)(cipherKey[idx]);
		sCipherKey = sCipherKey |((uint16_t)(cipherKey[idx + 1]) << 8);
		AESAKEY = sCipherKey;
	}

	/* Wait until key is written */
	while((AESASTAT & AESKEYWR ) == 0);
}

void resetAESAccelerator(void){
	AESACTL0 |= AESSWRST;
}

static void writeDataOp(const uint8_t (*src)[16], volatile uint16_t *reg){
	uint16_t temp, idx;

	// Key unchanged for DREAMS
	AESASTAT |= AESKEYWR;

	// Write data to register
	for(idx = 0; idx < 16; idx = idx + 2){
		temp = (uint16_t)(*src)[idx];
		temp = temp | (uint16_t)((*src)[idx + 1] << 8);
		*reg = temp;
	}
}

static void readDataOp(uint8_t (*dst)[16]){
	uint16_t temp, idx;

	for(idx = 0; idx < 16; idx = idx + 2){
		temp = AESADOUT;

		(*dst)[idx] = (uint8_t)temp;
		(*dst)[idx+1] = (uint8_t)(temp >> 8);
	}
}

void startEncryptData(const uint8_t (*data)[16]){
	// Check key has been written
	if(((AESASTAT & AESKEYWR) != AESKEYWR) || keyChanged){
		setupAESAccelerator();
		keyChanged = false;
	}

	// Set to encrypt mode
	AESACTL0 = (AESACTL0 & ~AESOP_3) | AESOP_0;

	// Write
	writeDataOp(data, &AESADIN);
}

void startDecryptData(const uint8_t (*encryptedData)[16]){
	// Check key has been written
	if(((AESASTAT & AESKEYWR) != AESKEYWR) || keyChanged){
		setupAESAccelerator();
		keyChanged = false;
	}

	// Set decrypt mode
	AESACTL0 = (AESACTL0 & ~AESOP_3) | AESOP_1;

	// Write
	writeDataOp(encryptedData, &AESADIN);
}

bool readData(uint8_t (*data)[16]){
	bool ret = false;
	if(!isAESAcceleratorBusy()){
		readDataOp(data);
		ret = true;
	}
	return ret;
}

bool isAESAcceleratorBusy(void){
	return (AESASTAT & AESBUSY) ? true : false;
}

void encryptData(const uint8_t (*data)[16], uint8_t (*encryptedData)[16]){
	startEncryptData(data);
	while(AESASTAT & AESBUSY);
	readDataOp(encryptedData);
}

void decryptData(const uint8_t (*encryptedData)[16], uint8_t (*data)[16]){
	startDecryptData(encryptedData);
	while(AESASTAT & AESBUSY);
	readDataOp(data);
}

uint8_t *encrypt(const uint8_t *data, uint16_t *len, const uint8_t *iv){
	uint16_t mod = (*len) & 0x000F,idxLen = *len >> 3,
			newLen = (idxLen << 3) + (mod==0 ? 0 : 16), idx, jdx;

	uint8_t src[16], dst[16], cbc[16];
	uint8_t *ret = (uint8_t*)malloc(newLen*sizeof(uint8_t));
	if(ret == NULL) // Allocation failed
		return NULL;

	if(iv == NULL){
		memset(cbc, '\0', sizeof(cbc));
		// Encrypt and copy over all the 16 word blocks of data
		for(idx = 0; idx < idxLen; idx = idx + 2){
			memcpy(src, (data + (idx << 3)), sizeof(src));
			encryptData((const uint8_t (*)[16])&src, &dst);
			memcpy((ret + (idx << 3)), dst, sizeof(dst));
		}
	}
	else{
		memcpy(cbc, iv, sizeof(cbc));
		// Encrypt and copy over all the 16 word blocks of data
		for(idx = 0; idx < idxLen; idx = idx + 2){
			memcpy(src, (data + (idx << 3)), sizeof(src));
			for(jdx = 0; jdx < 16; jdx++) // CBC
				src[jdx] = src[jdx] ^ cbc[jdx];
			encryptData((const uint8_t (*)[16])&src, &dst);
			memcpy((ret + (idx << 3)), dst, sizeof(dst));
			memcpy(cbc, dst, sizeof(cbc));
		}
	}

	// Encrypt and copy over the final blocks
	if(mod != 0){
		memset(src, '\0', 16*sizeof(uint8_t));
		memcpy(src, data + *len - mod, mod*sizeof(uint8_t));
		for(jdx = 0; jdx < 16; jdx++) // CBC (zeroed if NULL IV)
			src[jdx] = src[jdx] ^ cbc[jdx];
		encryptData((const uint8_t (*)[16])&src, &dst);
		memcpy(ret + *len - mod, dst, sizeof(dst));
	}

	*len = newLen;
	return ret;
}

uint8_t *decrypt(const uint8_t *encryptedData, const uint16_t len, const uint8_t *iv){
	uint16_t idxLen = len >> 3, idx, jdx;

	uint8_t src[16], dst[16], cbc[16];
	uint8_t *ret = (uint8_t*)malloc(len*sizeof(uint8_t));
	if(ret == NULL) // Allocation failed
		return NULL;

	if(iv == NULL){
		// Decrypt and copy over all the 16 word blocks of data
		for(idx = 0; idx < idxLen; idx = idx + 2){
			memcpy(src, (encryptedData + (idx << 3)), sizeof(src));
			decryptData((const uint8_t (*)[16])&src, &dst);
			memcpy((ret + (idx << 3)), dst, sizeof(dst));
		}
	}
	else{
		memcpy(cbc, iv, sizeof(cbc));
		for(idx = 0; idx < idxLen; idx = idx + 2){
			memcpy(src, (encryptedData + (idx << 3)), sizeof(src));
			decryptData((const uint8_t (*)[16])&src, &dst);
			for(jdx = 0; jdx < 16; jdx++) // CBC
				dst[jdx] = dst[jdx] ^ cbc[jdx];
			memcpy((ret + (idx << 3)), dst, sizeof(dst));
			memcpy(cbc, src, sizeof(cbc));
		}
	}
	return ret;
}
