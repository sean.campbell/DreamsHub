/*
 * scheduler.h
 *
 *  Created on: 12 Feb 2016
 *      Author: David
 */

#ifndef LIB_SCHEDULER_H_
#define LIB_SCHEDULER_H_

#include "driverlib.h"

#include <stdbool.h>
#include <stdint.h>

// Function pointer represnting a task
typedef void (*taskCB)(int_fast32_t a);

void scheduler_init(void);

void scheduler_start(void);

void scheduler_stop(void);

void scheduler_destroy();

bool scheduler_containsTask(uint_fast32_t id);

// Add a task to scheduler
bool scheduler_addTask(taskCB task, uint_fast32_t id, uint_fast32_t period, int_fast32_t arg);

bool scheduler_editTaskPeriod(uint_fast32_t id, uint_fast32_t newPeriod);

bool scheduler_deleteTask(uint_fast32_t id);

// List number of tasks
uint_fast32_t scheduler_taskCount(void);

void scheduler_perform(void);

void systick_isr(void);


#endif /* LIB_SCHEDULER_H_ */
