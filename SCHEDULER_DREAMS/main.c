#include "driverlib.h"

#include "hub_DREAMS.h"
#include "scheduler.h"

int main(void) {

	WDT_A_holdTimer();

	scheduler_init();

	initHub();

	while (true) {
		scheduler_perform();
	}

	//scheduler_destroy();
}

