/*
 * scheduler.c
 *
 *  Created on: 12 Feb 2016
 *      Author: David
 */
#include "LinkedList.h"

#include "scheduler.h"
#include "driverlib.h"

#include "clocking_DREAMS.h"

#include <stdlib.h>

#ifndef NULL
#define NULL ((void*)0)
#endif

// Peroid of tick in s
#define PERIOD_S 0.01

// Structure defining a task
typedef struct taskS {
	uint_fast32_t id;
	taskCB task; // Pointer to the function be carried out
	uint_fast32_t period; // Period of task
	int_fast32_t arg;
	bool perform;
} taskS;

// Private info
// Task information
static LinkedList *taskList;

// Public functions
void scheduler_init(void){
	taskList = emlist_create();
}

void scheduler_destroy(void){
	emlist_destroy(taskList);
}

void scheduler_start(void){
	// Period in clock cycles
	static uint_fast32_t tickPeriod = MCLK_RATE * PERIOD_S;

	// Enable SysTick
	MAP_SysTick_enableModule();
	/// Set period, 30000 @ 3MHz = 0.01s
	MAP_SysTick_setPeriod(tickPeriod);

	MAP_SysTick_enableInterrupt();
	// Enable Master Interrupt
	MAP_Interrupt_enableMaster();
}

void scheduler_stop(void){
	MAP_SysTick_disableInterrupt();
	MAP_SysTick_disableModule();
}

static taskS *findTask(uint_fast32_t id){
	taskS *ret = NULL, *temp;
	LinkedListElement *elem = NULL;
	LinkedListIterator iter = emlist_iterator(taskList);
	while((elem = emlist_iterator_next(&iter)) != NULL){
		temp = ((taskS*)elem->value);
		if(temp->id == id){
			ret = temp;
			break;
		}
	}
	return ret;
}

bool scheduler_containsTask(uint_fast32_t id){
	return (findTask(id) != NULL);
}

bool scheduler_addTask(taskCB task, uint_fast32_t id, uint_fast32_t period, int_fast32_t arg){
	bool ret = false;
	// Convert period in s to period in cycles
	uint_fast32_t task_period = (period / (float)PERIOD_S);
	taskS *newTask = malloc(sizeof(taskS));
	if(newTask != NULL){
		newTask->id = id;
		newTask->task = task;
		newTask->period = task_period;
		newTask->arg = arg;
		newTask->perform = false;

		ret = emlist_insert(taskList, (void*)newTask);
	}
	return ret;
}

bool scheduler_editTaskPeriod(uint_fast32_t id, uint_fast32_t newPeriod){
	bool ret = false;
	taskS *task = findTask(id);
	if(task != NULL){
		task->period = (newPeriod / (float)PERIOD_S);
		ret = true;
	}
	return ret;
}

static volatile bool delOccurred = false;

bool scheduler_deleteTask(uint_fast32_t id){
	bool ret = false;
	taskS *task = findTask(id);

	if(task != NULL){
		ret = emlist_remove(taskList, task);
		if(ret){
			task->task = NULL;
			task->perform = false;

			free(task);
			task = NULL;
			delOccurred = true;
		}
	}
	return ret;
}

// Return the number of tasks currently scheduled
uint_fast32_t scheduler_taskCount(){
	return emlist_size(taskList);
}

void scheduler_perform(void){
	LinkedListElement *elem = NULL;
	LinkedListIterator iter = emlist_iterator(taskList);
	while((elem = emlist_iterator_next(&iter)) != NULL && !delOccurred){
		taskS *task = ((taskS*)elem->value);
		if(task->perform){
			task->task(task->arg);
			task->perform = false;
		}
	}
	if(delOccurred){
		delOccurred = false;
	}
}
// Interrupt Service routine for systick
void systick_isr(void) {
	// Count of ticks
	static uint_fast32_t tickCount = 0;

	// Increment tick count
	tickCount++;
	tickCount %= 1000000;

	LinkedListElement *elem = NULL;
	LinkedListIterator iter = emlist_iterator(taskList);
	while((elem = emlist_iterator_next(&iter)) != NULL){
		taskS *task = ((taskS*)elem->value);
		if((tickCount % task->period) == 0){
			task->perform = true;
		}
	}
}
