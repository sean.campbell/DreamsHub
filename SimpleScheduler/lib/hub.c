/*
 * hub.c
 *
 *  Created on: 11 Feb 2016
 *      Author: David
 */
#include "log_DREAMS.h"
#include "hub.h"
#include "GenericCircBuff.h"
#include <stdlib.h>
#include <string.h>

#define DREAMS_SERVER_HOSTNAME 	"dreams.3utilities.com"
//6666
#define DREAMS_SERVER_PORT		"2389"

#define HUB_SITE_ID				2

static circBuffS readingBuff;

// Buffer for TCP messages from server
#define TCP_RECEIVE_BUFFER_SIZE	256
static char tcpReceiveBuffer[TCP_RECEIVE_BUFFER_SIZE];

#define EMERGENCY_SMS_ABOVE "Warning: Sensor%d at SiteID %d exceeded maximum threshold of %.5f with value %.5f"
#define EMERGENCY_SMS_BELOW "Warning: Sensor%d at SiteID %d below minimum threshold of %.5f with value %.5f"

static void checkThresholds(adcSensorSettings *settings, float value){
	bool toSend = false;
	if(!settings->belowLowerThreshold && value < settings->lowerThreshold){
		snprintf(tcpReceiveBuffer, TCP_RECEIVE_BUFFER_SIZE, EMERGENCY_SMS_BELOW,
				settings->sensorID, HUB_SITE_ID, settings->lowerThreshold, value);
		settings->belowLowerThreshold = true;
		settings->aboveUpperThreshold = false;
		toSend = true;
	}
	else if(!settings->aboveUpperThreshold && value > settings->upperThreshold){
		snprintf(tcpReceiveBuffer, TCP_RECEIVE_BUFFER_SIZE, EMERGENCY_SMS_ABOVE,
						settings->sensorID, HUB_SITE_ID, settings->upperThreshold, value);
		settings->aboveUpperThreshold = true;
		settings->belowLowerThreshold = false;
		toSend = true;
	}
	// Blanket send to all numbers recorded on the SIM
	if(toSend){
		modem_sendEmergencySMS(tcpReceiveBuffer);
	}
}

void readADC(int sensorId) {
	adcSensorSettings *settings = NULL;
	// Get settings and if valid, ensure enabled
	if((settings = adc_getSensorSettings(sensorId)) != NULL && settings->sensorEn){
		// Read the sensor value
		float result = adc_readSensor(sensorId);
		//checkThresholds(settings, result);

		// Fetch the time
		timeS currTime;
		getTime(&currTime);

		// Build the buffer entry
		union buffU bufferEntry;
		readingS reading = {settings->sensorID, currTime, result};
		bufferEntry.reading = reading;
		circBuffS_put(&readingBuff, bufferEntry);
	}
}

void logToSD(char *data){
	int bw = log_writeLine(LOG_1_NAME, data);
	if(bw > 0){
		toggleLED2(0);
	}
}

void logToServer(char *data){
	toggleLED1(0);
	if(modem_openTCPConnection(DREAMS_SERVER_HOSTNAME, DREAMS_SERVER_PORT)){
		modem_sendTCPMessage(data);
		// TODO Use Comms layer abstraction for sending and receiving responses
	}
	toggleLED1(0);
}

#define MESSAGE_START 	"{\"SiteID\":%d, \"Readings\":["
#define MESSAGE_TIMESTAMP	"%04d-%02d-%02dT%02d:%02d:%02d"
#define MESSAGE_READING	"{\"TimeStamp\":\"%s\", \"sensor%02d\":%.5f}"
#define MESSAGE_END		"]}"

#define BUFFER_LENGTH	2048
#define MAX_LENGTH BUFFER_LENGTH - (20 + 44 + 3)	// 44 assumes 5 decimal points + several preceding
static char data[BUFFER_LENGTH], time[22], temp[128];
void log(int arg1){
	scheduler_stop();

	snprintf(data, sizeof(data), MESSAGE_START, HUB_SITE_ID);
	uint_fast32_t numBytes = strlen(data);

	while(!circBuffS_empty(&readingBuff) && numBytes < MAX_LENGTH){
		readingS val = circBuffS_get(&readingBuff).reading;

		// ISO time YYYY-MM-DDTHH:mm:ss
		snprintf(time, sizeof(time), MESSAGE_TIMESTAMP,
			val.time.yr,
			val.time.mth,
			val.time.day,
			val.time.hr,
			val.time.min,
			val.time.sec);

		snprintf(temp, sizeof(temp), MESSAGE_READING,
			time,
			val.sensorID,
			val.value);

		if(!circBuffS_empty(&readingBuff)) strcat(temp, ",");
		strcat(data, temp);
		numBytes += strlen(temp);
	}

	strcat(data, MESSAGE_END);

	logToSD(&data[0]);
	logToServer(&data[0]);

	scheduler_init();
}

void initADC() {
	// Call relevant init
	adc_init();

	// Iterate through the settings and set settings if enabled
	uint_fast32_t idx = 0;
	adcSensorSettings *settings = NULL;
	while((settings = adc_getSensorSettings(idx)) != NULL){
		if(settings->sensorEn){
			scheduler_addTask(readADC, settings->sampleRate, idx);
		}
		idx++;
	}
}

void toggleLED0(int a) {
	MAP_GPIO_toggleOutputOnPin(LED0_PORT, LED0_PIN);
}
void toggleLED1(int a) {
	MAP_GPIO_toggleOutputOnPin(LED1_PORT, LED1_PIN);
}
void toggleLED2(int a) {
	MAP_GPIO_toggleOutputOnPin(LED2_PORT, LED2_PIN);
}

void initLEDs() {
	MAP_GPIO_setAsOutputPin(LED0_PORT, LED0_PIN);
	MAP_GPIO_setAsOutputPin(LED1_PORT, LED1_PIN);
	MAP_GPIO_setAsOutputPin(LED2_PORT, LED2_PIN);

	scheduler_addTask(toggleLED0, 2, 0);
}

void initModem() {
	modem_powerOn();
	// Ensure the modem is off the network
	modem_closeTCPConnection(true);

	// Try several times to connect to the server on first power up
	bool connected = false;
	uint_fast8_t retry = 0;
	while(retry < 3 && !connected){
		connected = modem_openTCPConnection(DREAMS_SERVER_HOSTNAME, DREAMS_SERVER_PORT);
		timer32_delay(100);
		retry++;
	}

	// If connected, perform the startup protocol
	if(connected){
		//comms_setupConfig(temp, sizeof(temp), tcpReceiveBuffer, TCP_RECEIVE_BUFFER_SIZE, HUB_SITE_ID);
	}
}

void initHub(){
    /* Enabling the FPU for floating point operation */
    MAP_FPU_enableModule();
    MAP_FPU_enableLazyStacking();

	circBuffS_init(&readingBuff);

	setupClocks();

	initRTC();

	initADC();

	initLEDs();

	// Initialise SD card and logging capabilities
	int res = log_init();
	if(res) log_createNew(LOG_1_PATH, LOG_1_NAME);
	scheduler_addTask(log, LOG_RATE, 0);

	initModem();

	scheduler_init();
}
