/*
 * hub.h
 *
 *  Created on: 10 Feb 2016
 *      Author: David
 */

#ifndef LIB_HUB_H_
#define LIB_HUB_H_

#include "driverlib.h"
#include "scheduler.h"

#include "adc_DREAMS.h"

#include "modem_DREAMS.h"
#include "DS1307.h"

#include "clocking_DREAMS.h"

#include <stdio.h>

/********************** Defines for pinout to HUB PCB*****************************/
// LED 0
#define LED0_PORT GPIO_PORT_P9
#define LED0_PIN GPIO_PIN3
// LED 1
#define LED1_PORT GPIO_PORT_P6
#define LED1_PIN GPIO_PIN3
// LED 2
#define LED2_PORT GPIO_PORT_P7
#define LED2_PIN GPIO_PIN2

#define MAX_RES_LEN 10

#define LOG_RATE 120
#define LOG_1_NAME "sensors"
#define LOG_1_PATH "/testRun"

typedef struct readingS
{
	uint8_t sensorID;
	timeS time;
	float value;
} readingS;

void readSensor(int sensorId);

void logData(int arg1);

void initHub();

void toggleLED0(int a);
void toggleLED1(int a);
void toggleLED2(int a);

#endif /* LIB_HUB_H_ */
