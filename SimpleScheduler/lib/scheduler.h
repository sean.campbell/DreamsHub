/*
 * scheduler.h
 *
 *  Created on: 12 Feb 2016
 *      Author: David
 */

#ifndef LIB_SCHEDULER_H_
#define LIB_SCHEDULER_H_

#include "driverlib.h"

#define MAXTASKS 6

// Peroid of tick in s
#define PERIOD_S 0.01
// Function pointer represnting a task
typedef void (*taskCB)(int a);

// Structure defining a task
typedef struct taskS
{
  taskCB task; // Pointer to the function be carried out
  int period; // Period of task
  int arg;
  bool perform;
} taskS;

// Initilise Scheduler and background hardware
void scheduler_init();

void scheduler_start(void);

void scheduler_stop(void);

// Add a task to scheduler
int scheduler_addTask(taskCB task, int period, int arg);

// List number of tasks
int scheduler_taskCount(void);

void scheduler_perform(void);

void systick_isr(void);


#endif /* LIB_SCHEDULER_H_ */
