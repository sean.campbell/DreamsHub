/*
 * scheduler.c
 *
 *  Created on: 12 Feb 2016
 *      Author: David
 */

#include "scheduler.h"
#include "driverlib.h"
#include "clocking_DREAMS.h"

// List of tasks
taskS taskList[MAXTASKS];
int nTasks = 0;
// Count of ticks
uint32_t tickCount = 0;

// Period in clock cycles
int tickPeriod = MCLK_RATE * PERIOD_S;

bool go = false;



void scheduler_init(){
	// Enable SysTick
	MAP_SysTick_enableModule();
	/// Set period, 30000 @ 3MHz = 0.01s
	MAP_SysTick_setPeriod(tickPeriod);


	MAP_SysTick_enableInterrupt();
	// Enable Master Interrupt
	MAP_Interrupt_enableMaster();
}

void scheduler_start(void){
	go = true;
}

void scheduler_stop(void){
	MAP_SysTick_disableInterrupt();
	MAP_SysTick_disableModule();
}

int scheduler_addTask(taskCB task, int period, int arg){

	// Convert period in s to period in cycles
	int task_period = (period / (float)PERIOD_S);
	if(nTasks < MAXTASKS){
		taskList[nTasks].arg = arg;
		taskList[nTasks].task = task;
		taskList[nTasks].period = task_period;
		taskList[nTasks].perform = false;
		nTasks++;
	}
	else{
		return 0;
	}

	return 1;
}

// Return the number of tasks currently scheduled
int scheduler_taskCount(){
	return nTasks;
}

void scheduler_perform(void){

	int i = 0;
	for (i = 0; i < nTasks; i++) {
		if(taskList[i].perform){
			taskList[i].task(taskList[i].arg);
			taskList[i].perform = false;
		}
	}
}
// Interrupt Service routine for systick
void systick_isr(void) {

	// Increment tick count
	tickCount++;
	tickCount %= 1000000;

	int i = 0;

	for (i = 0; i < nTasks; i++) {
		if ((tickCount % taskList[i].period) == 0) {
			taskList[i].perform = true;
		}
	}
}
