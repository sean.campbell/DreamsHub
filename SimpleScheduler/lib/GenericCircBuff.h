 #ifndef __circBuffS_H
    #define __circBuffS_H
    #define BUFF_SIZE    256

#include "hub.h"

	union buffU{
		uint8_t uChar;
		readingS reading;
	};

	/* This struct contains all the relevent information about a
	 * circular buffer/Queue.
	 */
    typedef struct  genericCircBuffS
    {
      union buffU buf[BUFF_SIZE]; // The actual buffer
      int head; // Position of first item in buffer
      int tail; // Position of last item in buffer
      int count; // Number of items in the buffer
    } circBuffS;


    /* Initialise the buffer.
     *
     * _this  Pointer to the buffer struct to be operated on
     */
	void  circBuffS_init  (circBuffS *_this);

    /* Function returns a flag depending on whether the buffer contains any items.
     *
     * args:
     * _this  Pointer to the buffer struct to be operated on
     *
     * returns:
     * Empty flag, 0 = not empty, 1 = empty.
     */
	int   circBuffS_empty (circBuffS *_this);

    /* Function returns a flag depending on whether the buffer if full.
     *
     * args:
     * _this  Pointer to the buffer struct to be operated on
     *
     * returns:
     * Full flag, 0 = not full, 1 = full.
     */
	int   circBuffS_full  (circBuffS *_this);

    /* Gets the first item from the buffer. i.e. Head of the queue.
     * This should be implemented atomically.
     *
     * args:
     * _this  Pointer to the buffer struct to be operated on
     *
     * returns:
     * Next character in buffer
     *
     */
	union buffU   circBuffS_get   (circBuffS *_this);

    /* Adds an item onto the end of the buffer. i.e. new tail of the queue.
     * This should be implemented atomically.
     *
     * args:
     * _this  Pointer to the buffer struct to be operated on
     * c  Character to be added into the buffer
     */
	void  circBuffS_put   (circBuffS *_this, union buffU c);

    /* Function which will reset the buffer head, tail and count, i
     * includes the option to flush the buffer.
     *
     * args:
     * _this  Pointer to the buffer struct to be operated on
     * clear  flag deciding whether memory should be cleared using memset. 0=no, 1=yes
     */
	void  circBuffS_flush  (circBuffS *_this, const int clear);

#endif
