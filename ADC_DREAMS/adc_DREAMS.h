#ifndef ADC_DREAMS_H_
#define ADC_DREAMS_H_

#include <stdint.h>
#include <stdbool.h>

typedef struct adcS {
	bool sensorEn;
	uint_fast32_t sensorID;
	uint_fast32_t sampleRate;
	uint_fast32_t memLoc;
	uint_fast32_t adcChannel;
	uint_fast32_t port;
	uint_fast32_t pin;
	float offset;
	float multiplier;

	float upperThreshold;
	bool aboveUpperThreshold;

	float lowerThreshold;
	bool belowLowerThreshold;
} adcSensorSettings;

void adc_init();
void adc_initOne(uint_fast32_t idx);

float adc_readSensor(uint_fast32_t idx);

adcSensorSettings *adc_getSensorSettings(uint_fast32_t idx);

#endif /* CLOCKING_DREAMS_H_ */
