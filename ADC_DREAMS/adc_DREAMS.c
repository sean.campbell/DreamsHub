#include "driverlib.h"
#include "adc_DREAMS.h"

#include <float.h>

#ifndef NULL
#define NULL ((void*)0)
#endif

// Default settings loaded for sensors
// Sensor 0 Analogue IN 0
#define SEN0_A0_EN 					0
#define SEN0_A0_ID 					0
#define SEN0_A0_PORT 				GPIO_PORT_P9
#define SEN0_A0_PIN 				GPIO_PIN1
#define SEN0_A0_P 					60
#define SEN0_A0_MEMLOC 				ADC_MEM16
#define SEN0_A0_CHAN 				ADC_INPUT_A16
#define SEN0_A0_OFFSET 				-0.5
#define SEN0_A0_MULT 				100
#define SEN0_A0_UPP 				FLT_MAX
#define SEN0_A0_LOW 				-FLT_MAX

// Sensor 0 Analogue IN 1
#define SEN0_A1_EN 					0
#define SEN0_A1_ID 					1
#define SEN0_A1_PORT 				GPIO_PORT_P8
#define SEN0_A1_PIN 				GPIO_PIN3
#define SEN0_A1_P 					30
#define SEN0_A1_MEMLOC 				ADC_MEM22
#define SEN0_A1_CHAN 				ADC_INPUT_A22
#define SEN0_A1_OFFSET 				-0.5
#define SEN0_A1_MULT 				100
#define SEN0_A1_UPP 				FLT_MAX
#define SEN0_A1_LOW 				-FLT_MAX

// Sensor 1 Analogue IN 0
#define SEN1_A0_EN 					0
#define SEN1_A0_ID 					2
#define SEN1_A0_PORT 				GPIO_PORT_P6
#define SEN1_A0_PIN 				GPIO_PIN0
#define SEN1_A0_P 					60
#define SEN1_A0_MEMLOC 				ADC_MEM15
#define SEN1_A0_CHAN 				ADC_INPUT_A15
#define SEN1_A0_OFFSET 				-1.65
#define SEN1_A0_MULT 				100.0/4.7
#define SEN1_A0_UPP 				20.0
#define SEN1_A0_LOW 				10.0

// Sensor 1 Analogue IN 1
#define SEN1_A1_EN 					0
#define SEN1_A1_ID 					3
#define SEN1_A1_PORT 				GPIO_PORT_P4
#define SEN1_A1_PIN 				GPIO_PIN3
#define SEN1_A1_P 					30
#define SEN1_A1_MEMLOC 				ADC_MEM10
#define SEN1_A1_CHAN 				ADC_INPUT_A10
#define SEN1_A1_OFFSET 				-0.5
#define SEN1_A1_MULT 				100
#define SEN1_A1_UPP 				FLT_MAX
#define SEN1_A1_LOW 				-FLT_MAX

// Sensor 2 Analogue IN 0
#define SEN2_A0_EN 					0
#define SEN2_A0_ID 					4
#define SEN2_A0_PORT 				GPIO_PORT_P4
#define SEN2_A0_PIN 				GPIO_PIN0
#define SEN2_A0_P 					60
#define SEN2_A0_MEMLOC 				ADC_MEM13
#define SEN2_A0_CHAN 				ADC_INPUT_A13
#define SEN2_A0_OFFSET				-0.5
#define SEN2_A0_MULT 				100
#define SEN2_A0_UPP 				FLT_MAX
#define SEN2_A0_LOW 				-FLT_MAX

// Sensor 2 Analogue IN 1
#define SEN2_A1_EN 					0
#define SEN2_A1_ID 					5
#define SEN2_A1_PORT 				GPIO_PORT_P6
#define SEN2_A1_PIN 				GPIO_PIN1
#define SEN2_A1_P 					60
#define SEN2_A1_MEMLOC 				ADC_MEM14
#define SEN2_A1_CHAN 				ADC_INPUT_A14
#define SEN2_A1_OFFSET 				0
#define SEN2_A1_MULT 				1.0
#define SEN2_A1_UPP 				FLT_MAX
#define SEN2_A1_LOW 				-FLT_MAX

// Sensor 3 Analogue IN 0
#define SEN3_A0_EN 					0
#define SEN3_A0_ID 					6
#define SEN3_A0_PORT 				GPIO_PORT_P4
#define SEN3_A0_PIN 				GPIO_PIN4
#define SEN3_A0_P 					60
#define SEN3_A0_MEMLOC 				ADC_MEM9
#define SEN3_A0_CHAN 				ADC_INPUT_A9
#define SEN3_A0_OFFSET 				0
#define SEN3_A0_MULT 				1.0
#define SEN3_A0_UPP 				FLT_MAX
#define SEN3_A0_LOW 				-FLT_MAX

// Sensor 3 Analogue IN 1
#define SEN3_A1_EN 					0
#define SEN3_A1_ID 					7
#define SEN3_A1_PORT 				GPIO_PORT_P4
#define SEN3_A1_PIN 				GPIO_PIN2
#define SEN3_A1_P 					60
#define SEN3_A1_MEMLOC 				ADC_MEM11
#define SEN3_A1_CHAN 				ADC_INPUT_A11
#define SEN3_A1_OFFSET 				0
#define SEN3_A1_MULT 				1.0
#define SEN3_A1_UPP 				FLT_MAX
#define SEN3_A1_LOW 				-FLT_MAX

// Sensor 4 Analogue IN 0
#define SEN4_A0_EN 					0
#define SEN4_A0_ID 					8
#define SEN4_A0_PORT 				GPIO_PORT_P4
#define SEN4_A0_PIN 				GPIO_PIN7
#define SEN4_A0_P 					60
#define SEN4_A0_MEMLOC 				ADC_MEM6
#define SEN4_A0_CHAN 				ADC_INPUT_A6
#define SEN4_A0_OFFSET 				0
#define SEN4_A0_MULT 				1.0
#define SEN4_A0_UPP 				FLT_MAX
#define SEN4_A0_LOW 				-FLT_MAX

// Sensor 4 Analogue IN 1
#define SEN4_A1_EN 					0
#define SEN4_A1_ID 					9
#define SEN4_A1_PORT 				GPIO_PORT_P4
#define SEN4_A1_PIN 				GPIO_PIN5
#define SEN4_A1_P 					60
#define SEN4_A1_MEMLOC 				ADC_MEM8
#define SEN4_A1_CHAN 				ADC_INPUT_A8
#define SEN4_A1_OFFSET 				0
#define SEN4_A1_MULT 				1.0
#define SEN4_A1_UPP 				FLT_MAX
#define SEN4_A1_LOW 				-FLT_MAX

static adcSensorSettings adcSensors[] = {
	{ SEN0_A0_EN, SEN0_A0_ID, SEN0_A0_P, SEN0_A0_MEMLOC, SEN0_A0_CHAN, SEN0_A0_PORT,
		SEN0_A0_PIN, SEN0_A0_OFFSET, SEN0_A0_MULT, SEN0_A0_UPP, false, SEN0_A0_LOW, false },
		{ SEN0_A1_EN, SEN0_A1_ID, SEN0_A1_P, SEN0_A1_MEMLOC, SEN0_A1_CHAN, SEN0_A1_PORT,
			SEN0_A1_PIN, SEN0_A1_OFFSET, SEN0_A1_MULT, SEN0_A1_UPP, false, SEN0_A1_LOW, false },

	{ SEN1_A0_EN, SEN1_A0_ID, SEN1_A0_P, SEN1_A0_MEMLOC, SEN1_A0_CHAN, SEN1_A0_PORT,
		SEN1_A0_PIN, SEN1_A0_OFFSET, SEN1_A0_MULT, SEN1_A0_UPP, false, SEN1_A0_LOW, false },
		{ SEN1_A1_EN, SEN1_A1_ID, SEN1_A1_P, SEN1_A1_MEMLOC, SEN1_A1_CHAN, SEN1_A1_PORT,
			SEN1_A1_PIN, SEN1_A1_OFFSET, SEN1_A1_MULT, SEN1_A1_UPP, false, SEN1_A1_LOW, false },

	{ SEN2_A0_EN, SEN2_A0_ID, SEN2_A0_P, SEN2_A0_MEMLOC, SEN2_A0_CHAN, SEN2_A0_PORT,
			SEN2_A0_PIN, SEN2_A0_OFFSET, SEN2_A0_MULT, SEN2_A0_UPP, false, SEN2_A0_LOW, false },
			{ SEN2_A1_EN, SEN2_A1_ID, SEN2_A1_P, SEN2_A1_MEMLOC, SEN2_A1_CHAN, SEN2_A1_PORT,
				SEN2_A1_PIN, SEN2_A1_OFFSET, SEN2_A1_MULT, SEN2_A1_UPP, false, SEN2_A1_LOW, false },

	{ SEN3_A0_EN, SEN3_A0_ID, SEN3_A0_P, SEN3_A0_MEMLOC, SEN3_A0_CHAN, SEN3_A0_PORT,
				SEN3_A0_PIN, SEN3_A0_OFFSET, SEN3_A0_MULT, SEN3_A0_UPP, false, SEN3_A0_LOW, false },
		{ SEN3_A1_EN, SEN3_A1_ID, SEN3_A1_P, SEN3_A1_MEMLOC, SEN3_A1_CHAN, SEN3_A1_PORT,
				SEN3_A1_PIN, SEN3_A1_OFFSET, SEN3_A1_MULT, SEN3_A1_UPP, false, SEN3_A1_LOW, false },

	{ SEN4_A0_EN, SEN4_A0_ID, SEN4_A0_P, SEN4_A0_MEMLOC, SEN4_A0_CHAN, SEN4_A0_PORT,
					SEN4_A0_PIN, SEN4_A0_OFFSET, SEN4_A0_MULT, SEN4_A0_UPP, false, SEN4_A0_LOW, false },
			{ SEN4_A1_EN, SEN4_A1_ID, SEN4_A1_P, SEN4_A1_MEMLOC, SEN4_A1_CHAN, SEN4_A1_PORT,
					SEN4_A1_PIN, SEN4_A1_OFFSET, SEN4_A1_MULT, SEN4_A1_UPP, false, SEN4_A1_LOW, false }
};

void adc_initOne(uint_fast32_t idx){
	uint_fast32_t nADC = sizeof(adcSensors)/sizeof(adcSensors[0]);

	if (idx >= nADC)
		return;

	MAP_ADC14_disableSampleTimer();

		if(adcSensors[idx].sensorEn) {
			/* Configuring GPIOs */
			MAP_GPIO_setAsPeripheralModuleFunctionInputPin(adcSensors[idx].port,
					adcSensors[idx].pin, GPIO_TERTIARY_MODULE_FUNCTION);

			// Configure all memory locations
			MAP_ADC14_configureConversionMemory(adcSensors[idx].memLoc,
					ADC_VREFPOS_AVCC_VREFNEG_VSS, adcSensors[idx].adcChannel, false);
		}

	MAP_ADC14_enableSampleTimer(ADC_MANUAL_ITERATION);
}

void adc_init(){
	uint_fast32_t idx = 0, nADC = sizeof(adcSensors)/sizeof(adcSensors[0]);

	/* Initializing ADC (MCLK/1/4) */
	MAP_ADC14_enableModule();
	MAP_ADC14_initModule(ADC_CLOCKSOURCE_MCLK, ADC_PREDIVIDER_1, ADC_DIVIDER_4, 0);

	/* Configuring the sample/hold time for TBD */
	MAP_ADC14_setSampleHoldTime(ADC_PULSE_WIDTH_192,ADC_PULSE_WIDTH_192);

	for(; idx != nADC; idx++){
		if(adcSensors[idx].sensorEn) {
			/* Configuring GPIOs */
			MAP_GPIO_setAsPeripheralModuleFunctionInputPin(adcSensors[idx].port,
					adcSensors[idx].pin, GPIO_TERTIARY_MODULE_FUNCTION);

			// Configure all memory locations
			MAP_ADC14_configureConversionMemory(adcSensors[idx].memLoc,
					ADC_VREFPOS_AVCC_VREFNEG_VSS, adcSensors[idx].adcChannel, false);
		}
	}

	MAP_ADC14_enableSampleTimer(ADC_MANUAL_ITERATION);
}

/**** FROM TI application notes******/
static float convertToFloat(uint16_t result) {
	return (result/16384.0f)*3.3f;
}

float adc_readSensor(uint_fast32_t idx){
	uint_fast16_t res;
	float result;

	// Disable ADC while we reconfigure it
	MAP_ADC14_disableConversion();

	// Configure single sample mode for this specific channel
	MAP_ADC14_configureSingleSampleMode(adcSensors[idx].memLoc, false);

	// Re-enable ADC
	MAP_ADC14_enableConversion();

	// Start conversion
	MAP_ADC14_toggleConversionTrigger();

	// Poll till finished
	while (MAP_ADC14_isBusy()) {
	}

	MAP_ADC14_clearInterruptFlag(adcSensors[idx].memLoc);

	// Get result
	res = MAP_ADC14_getResult(adcSensors[idx].memLoc);

	// Convert to Float
	result = convertToFloat(res);

	// Scale
	result += adcSensors[idx].offset;
	result *= adcSensors[idx].multiplier;

	return result;
}

adcSensorSettings *adc_getSensorSettings(uint_fast32_t idx){
	uint_fast32_t nADC = sizeof(adcSensors)/sizeof(adcSensors[0]);
	adcSensorSettings *ret = NULL;
	if(idx < nADC){
		ret = &(adcSensors[idx]);
	}
	return ret;
}

